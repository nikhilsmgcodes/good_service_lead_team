webpackJsonp(["main"],{

/***/ "./src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_gendir lazy recursive";

/***/ }),

/***/ "./src/app/addlead/addlead.component.css":
/***/ (function(module, exports) {

module.exports = "/* input,select,textarea{\n  border-style: solid;\n  border-color: #9b9b9b;\n} */\n"

/***/ }),

/***/ "./src/app/addlead/addlead.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid \">\n  <div class=\"row\">\n      <div class=\"col-lg-12\">\n          <div class=\"card\">\n              <div class=\"header\">\n                  <h4 class=\"title pull-left\">Add Lead</h4>\n                  <hr>\n                  <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n              </div>\n              <div class=\"content\">\n                <!-- <form> -->\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <label><b>Enter Customer Phone to Get the Address:</b></label>\n                    <input #addLeadFrom class=\"form-control border-input\" type=\"number\" [(ngModel)]=\"c_phone\" (ngModelChange)=\"searchByPhone()\" placeholder=\"Enter Phone Number\"/>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <br>\n                    <!-- <button class=\"btn btn-default\" (click)=\"searchByPhone()\" >Search</button> -->\n                    <!-- <b> OR </b>\n                    <button class=\"btn btn-primary\" (click)=\"addNewAddress()\">Add New Address</button> -->\n                  </div>\n                </div>\n                <br>\n                <div class=\"row\">\n                  <div class=\"\" *ngFor=\"let address of c_address;let i=index;\">\n                    <div class=\"col-md-6\">\n                      <div class=\"col-md-1\">\n                        <input type=\"radio\" value={{address.ca_id}} [(ngModel)]=\"selectedAddress\" #addLeadFrom>\n                      </div>\n                      <div class=\"col-md-5\">\n                      <span>\n                            <b>Address: </b>{{address.name}}<br> {{address.address}}, {{address.city}}, {{address.state}} - {{address.zip}}\n                        <br>Phone: {{address.optional_phone}}\n                        <br>Email: {{address.email}}\n                      </span>\n                    </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col-md-12\" [hidden]=\"!showDiv\">\n                  <br>\n                  <button class=\"btn btn-primary\" (click)=\"addNewAddress()\">Add New Address</button>\n                  <hr>\n                </div>\n\n                <div class=\"row\">\n                  <div class=\"col-md-6 form-group\">\n                    <label><b>Select Category: </b></label>\n                    <select [(ngModel)]=\"selectedCategory\" class=\"form-control border-input\">\n                      <option *ngFor=\"let c of category\" [ngValue]=\"c\">{{c}}</option>\n                    </select>\n                  </div>\n                  <div class=\"col-md-6 form-group\">\n                    <label><b>Select Brand: </b></label>\n                    <select [(ngModel)]=\"selectedBrand\" class=\"form-control border-input\">\n                      <option *ngFor=\"let b of brand;let i=index;\" [ngValue]=\"b\">{{b}}</option>\n                    </select>\n                  </div>\n                </div>\n                <div class=\"row\">\n                  <div class=\"col-md-12 form-group\">\n                    <label><b>Description: </b></label>\n                    <textarea class=\"form-control border-input\" rows=\"5\" [(ngModel)]=\"description\" placeholder=\"Enter Description...\"></textarea>\n                  </div>\n                </div>\n                <div class=\"row\">\n                  <div class=\"col-md-12 form-group\">\n                      <button class=\"btn btn-success\" (click)=\"addNewLead()\">Add Lead</button>\n                  </div>\n                </div>\n                </div>\n              <!-- </form> -->\n              </div>\n\n      </div>\n  </div>\n</div>\n\n<app-dialog [(visible)]=\"showDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Add New Address</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n                </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                    <!-- <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_phone\" type=\"tel\" name=\"\" value=\"\" placeholder=\"Customer Phone\" readonly>\n                    </div> -->\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_name\" type=\"text\" name=\"\" value=\"\" placeholder=\"Name\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_email\" type=\"email\" name=\"\" value=\"\" placeholder=\"Email (optional)\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <textarea class=\"form-control border-input\" type=\"text\" rows=\"3\" [(ngModel)]=\"c_adrs\" placeholder=\"Enter Address...\"></textarea>\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_city\" type=\"text\" name=\"\" value=\"\" placeholder=\"City\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_state\" type=\"text\" name=\"\" value=\"\" placeholder=\"State\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_zip\" type=\"number\" name=\"\" value=\"\" placeholder=\"Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"c_optional_phone\" type=\"number\" name=\"\" value=\"\" placeholder=\"Optional Phone\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" (click)=\"addAddress()\">Add Address</button>\n                    </div>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/addlead/addlead.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddleadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddleadComponent = (function () {
    function AddleadComponent(http, router) {
        this.http = http;
        this.router = router;
        //  @ViewChild('addLeadFrom') myForm: any;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.category = ['AC', 'TV', 'WASHING-MACHINE', 'REFRIGERATOR', 'CHIMNEY', 'WATER-PURIFIER', 'MICROWAVE-OVEN', 'GEYSER'];
        this.brand = ['LG', 'SONY', 'SAMSUNG', 'IFB', 'WHIRLPOOL', 'BOSCH', 'SIEMENS', 'VIDEOCON', 'ONIDA'];
        this.selectedAddress = null;
    }
    AddleadComponent.prototype.ngOnInit = function () {
        //this.selectedBrand = 'LG';
    };
    AddleadComponent.prototype.searchByPhone = function () {
        var _this = this;
        //var cphone = this.c_phone.toString();
        if (this.c_phone.toString().length == 10) {
            this.data = "c_phone=" + this.c_phone;
            this.httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                    'session-key': JSON.parse(localStorage.getItem('session_key'))
                })
            };
            this.http.post('http://' + this.url + 'getCustomerAddressByPhoneAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                        _this.c_address = null;
                        _this.showDiv = false;
                        if (data.message == 'Customer Data Not Found') {
                            _this.showDialog = true;
                        }
                        else {
                            //do nothing
                        }
                    }
                    else {
                        _this.c_address = data.data;
                        //alert(data.message);
                        console.log(_this.c_address);
                        _this.showDiv = true;
                    }
                }
            });
        }
        else {
            this.c_address = null;
            this.showDiv = false;
        }
    };
    AddleadComponent.prototype.addNewLead = function () {
        var _this = this;
        //let lt_phone = JSON.parse(localStorage.getItem('user_phone'));
        var ca_id = this.selectedAddress;
        var category = this.selectedCategory;
        var brand = this.selectedBrand;
        var description = this.description;
        this.data = { ca_id: ca_id, category: category, brand: brand, description: description };
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'addNewLeadAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    alert(data.message);
                    //  this.myForm.nativeElement.value = '';
                    _this.c_phone = null;
                    _this.c_address = null;
                    _this.selectedCategory = null;
                    _this.selectedBrand = null;
                    _this.description = null;
                    _this.showDiv = false;
                }
            }
        });
    };
    AddleadComponent.prototype.addNewAddress = function () {
        this.showDialog = true;
    };
    AddleadComponent.prototype.addAddress = function () {
        var _this = this;
        this.data = { c_phone: this.c_phone, name: this.c_name, email: this.c_email, address: this.c_adrs, city: this.c_city, state: this.c_state, zip: this.c_zip, optional_phone: this.c_optional_phone };
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'addNewCustomerAddressAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    alert(data.message);
                    _this.searchByPhone();
                    //this.c_phone = null;
                    _this.c_name = null;
                    _this.c_email = null;
                    _this.c_adrs = null;
                    _this.c_city = null;
                    _this.c_state = null;
                    _this.c_zip = null;
                    _this.c_optional_phone = null;
                    _this.showDialog = false;
                }
            }
        });
    };
    return AddleadComponent;
}());
AddleadComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-addlead',
        template: __webpack_require__("./src/app/addlead/addlead.component.html"),
        styles: [__webpack_require__("./src/app/addlead/addlead.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AddleadComponent);

var _a, _b;
//# sourceMappingURL=addlead.component.js.map

/***/ }),

/***/ "./src/app/allotmentteam/allotmentteam.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/allotmentteam/allotmentteam.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"card\">\n              <div class=\"header\">\n                  <h4 class=\"title pull-left\">Allotment Team</h4>\n                  <button class=\"btn-success\" style=\"float: right; color: white;\" (click)=\"showAddDialog()\" >Add New Member</button>\n                  <hr>\n                  <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n              </div>\n              <div class=\"content table-responsive table-full-width \" style=\"margin-left:1px; margin-right:1px;\">\n                  <table class=\"table table-striped\">\n                      <!-- <thead>\n                          <tr>\n                              <th *ngFor=\"let cell of tableData1.headerRow\">{{ cell }}</th>\n                          </tr>\n                      </thead> -->\n                      <td>S.No.</td>\n                      <!-- <td>JobId</td> -->\n                      <td>Phone</td>\n                      <td>Name</td>\n                      <td>Email</td>\n\n                      <tbody>\n                          <tr *ngFor=\"let row of allAllotmentTeam;let i=index;\">\n                              <!-- <p>{{ jobpostdetail?.length }}</p> -->\n                              <td>{{i+1}}</td>\n                              <!-- <td>{{row.job_id}}</td> -->\n                              <td>{{row.at_phone}}</td>\n                              <td>{{row.name}}</td>\n                              <td>{{row.email}}</td>\n                              <!-- <td>{{row.address1}}<br>{{row.address2}}<br>{{row.city}}<br>{{row.state}}<br>{{row.zip}}</td> -->\n                              <td>\n                                <button class=\"btn-info\" style=\"color: white;\" (click)=\"viewAllotmentTeamDetails(row.at_phone)\" >Details</button>\n                                <button class=\"btn-primary\" style=\"color: white;\" (click)=\"editAllotmentTeamDetails(row.at_phone)\" >Edit</button>\n                                <!-- <button style=\"background-color: #0095FF;color: white;\" (click)=\"editLead(row.l_id)\" >Edit</button> -->\n                                <button class=\"btn-danger\" style=\"color: white;\" (click)=\"deleteAllotmentTeam(row.at_phone)\" >Remove</button>\n                            </td>\n                          </tr>\n                      </tbody>\n                  </table>\n\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n<app-dialog [(visible)]=\"showDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Allotment Team Member Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                      <div class=\"col-md-3\">\n                        <div class=\"author\">\n                          <img class=\"avatar border-white\" src=\"{{at_details.photo}}\" alt=\"...\" style=\"width: 100px; height:  100px;\" />\n                        </div>\n                      </div>\n                      <div class=\"col-md-9\">\n                        <p><b>Phone:</b> {{at_details.at_phone}}</p>\n                        <p><b>Name:</b> {{at_details.name}}</p>\n                        <p><b>Address:</b> {{at_details.address}}, {{at_details.city}},\n                                    {{at_details.state}} - {{at_details.zip}}</p>\n                        <p><b>Password:</b> {{at_details.password}}</p>\n                      </div>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showEditDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Update Allotment Team Member Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                    <div class=\"col-md-3 form-group\">\n                      <div class=\"author\">\n                        <img class=\"avatar border-white\" src=\"{{at_details.photo}}\" alt=\"...\" style=\"width: 90px; height:  90px;\" />\n                      </div>\n                    </div>\n                    <div class=\"col-md-9 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.at_phone\" name=\"at_phone\"  type=\"number\" placeholder=\"Enter Phone Number\" readonly>\n                    </div>\n                    <div class=\"col-md-9 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.name\" name=\"at_name\" type=\"text\" placeholder=\"Enter Name\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.email\" name=\"at_email\" type=\"email\" placeholder=\"Enter Email\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.aadhaar\" name=\"at_aadhaar\" type=\"text\" placeholder=\"Enter Aadhaar\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <textarea class=\"form-control border-input\" [(ngModel)]=\"at_details.address\" name=\"at_address\" type=\"text\" rows=\"7\" placeholder=\"Enter Address\"></textarea>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.city\" name=\"at_city\" type=\"text\" placeholder=\"Enter City\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.state\" name=\"at_state\" type=\"text\" placeholder=\"Enter State\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.zip\" name=\"at_zip\" type=\"number\" placeholder=\"Enter Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_details.password\" name=\"at_password\" type=\"text\" placeholder=\"Enter Password\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" type=\"submit\" value=\"Submit\" (click)=\"updateAllotmentTeamMember()\">Update Details</button>\n                    </div>\n\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showAddNewDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Add New Allotment Team Member</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                   <form>\n                    <div class=\"col-md-12 form-group\">\n                      <input type=\"file\" placeholder=\"Choose Image\" name=\"image\" class=\"col-md-12\" (change)=\"fileChanged($event)\" #ProfilePic>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_phone\" name=\"at_phone\"  type=\"number\" placeholder=\"Enter Phone Number\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_name\" name=\"at_name\" type=\"text\" placeholder=\"Enter Name\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_email\" name=\"at_email\" type=\"email\" placeholder=\"Enter Email\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_aadhaar\" name=\"at_aadhaar\" type=\"text\" placeholder=\"Enter Aadhaar\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <textarea class=\"form-control border-input\" [(ngModel)]=\"at_address\" name=\"at_address\" type=\"text\" rows=\"8\" placeholder=\"Enter Address\"></textarea>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_city\" name=\"at_city\" type=\"text\" placeholder=\"Enter City\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_state\" name=\"at_state\" type=\"text\" placeholder=\"Enter State\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_zip\" name=\"at_zip\" type=\"number\" placeholder=\"Enter Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"at_password\" name=\"at_password\" type=\"text\" placeholder=\"Enter Password\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" type=\"submit\" value=\"Submit\" (click)=\"addNewAllotmentTeam()\">Add Member</button>\n                    </div>\n                  </form>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/allotmentteam/allotmentteam.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllotmentteamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AllotmentteamComponent = (function () {
    function AllotmentteamComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.at_details = { at_phone: null, name: null, address: null, city: null, state: null, zip: null, aadhaar: null, photo: null };
        this.at_phone = null;
    }
    AllotmentteamComponent.prototype.ngOnInit = function () {
        this.allotmentTeamMembers();
    };
    AllotmentteamComponent.prototype.allotmentTeamMembers = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'allotmentTeamMembers', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.allAllotmentTeam = null;
                }
                else {
                    _this.allAllotmentTeam = data.allotmentteam;
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    AllotmentteamComponent.prototype.viewAllotmentTeamDetails = function (at_phone) {
        var _this = this;
        this.showDialog = true;
        this.data = "at_phone=" + at_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'viewAllotmentTeamDetailsByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.at_details = null;
                }
                else {
                    _this.at_details = data.at_details[0];
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    AllotmentteamComponent.prototype.deleteAllotmentTeam = function (at_phone) {
        var _this = this;
        var check = confirm('Sure You want to Remove the Allotment Team Member?');
        if (!check) {
            //do nothing
        }
        else {
            this.data = "at_phone=" + at_phone;
            this.httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                    'session-key': JSON.parse(localStorage.getItem('session_key'))
                })
            };
            this.http.post('http://' + this.url + 'deleteAllotmentTeamByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                    }
                    else {
                        alert(data.message);
                        _this.allotmentTeamMembers();
                    }
                }
            });
        }
    };
    AllotmentteamComponent.prototype.editAllotmentTeamDetails = function (at_phone) {
        var _this = this;
        this.showEditDialog = true;
        this.data = "at_phone=" + at_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'viewAllotmentTeamDetailsByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.at_details = null;
                }
                else {
                    _this.at_details = data.at_details[0];
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    AllotmentteamComponent.prototype.showAddDialog = function () {
        this.showAddNewDialog = true;
    };
    AllotmentteamComponent.prototype.addNewAllotmentTeam = function () {
        var _this = this;
        var fileList = this.image.target.files;
        console.log(fileList);
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData = new FormData();
            formData.append('image', file, file.name);
            formData.append('at_phone', this.at_phone);
            formData.append('at_name', this.at_name);
            formData.append('at_email', this.at_email);
            formData.append('at_aadhaar', this.at_aadhaar);
            formData.append('at_address', this.at_address);
            formData.append('at_city', this.at_city);
            formData.append('at_state', this.at_state);
            formData.append('at_zip', this.at_zip);
            formData.append('at_password', this.at_password);
            //   this.httpOptions = {
            //     headers : new Headers({
            //     'Content-Type':  'application/json',
            //     'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            //     'session-key': JSON.parse(localStorage.getItem('session_key'))
            //   })
            // };
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Accept': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            });
            //headers.append('Accept', 'application/json');
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            this.http.post('http://' + this.url + 'addNewAllotmentTeam', formData, options).map(function (res) { return res.json(); }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].throw(error); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                        //this.at_details = null;
                    }
                    else {
                        alert(data.message);
                        _this.at_name = null;
                        _this.at_phone = null;
                        _this.at_email = null;
                        _this.at_aadhaar = null;
                        _this.at_address = null;
                        _this.at_city = null;
                        _this.at_state = null;
                        _this.at_zip = null;
                        _this.image = null;
                        _this.at_password = null;
                        _this.myInputVariable.nativeElement.value = "";
                        _this.allotmentTeamMembers();
                        _this.showAddNewDialog = false;
                    }
                }
            }, function (error) { return console.log(error); });
        }
        else {
            alert('Choose Image');
        }
    };
    AllotmentteamComponent.prototype.fileChanged = function (event) {
        this.image = event;
        console.log(this.image);
    };
    AllotmentteamComponent.prototype.updateAllotmentTeamMember = function () {
        var _this = this;
        //at_password = this.at_password;
        this.data = this.at_details, { at_password: this.at_password };
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'updateAllotmentTeamMember', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.at_details = null;
                }
                else {
                    //this.at_details=data.at_details[0];
                    alert(data.message);
                    _this.showEditDialog = false;
                    //console.log(this.c_address);
                }
            }
        });
    };
    return AllotmentteamComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ProfilePic'),
    __metadata("design:type", Object)
], AllotmentteamComponent.prototype, "myInputVariable", void 0);
AllotmentteamComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-allotmentteam',
        template: __webpack_require__("./src/app/allotmentteam/allotmentteam.component.html"),
        styles: [__webpack_require__("./src/app/allotmentteam/allotmentteam.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AllotmentteamComponent);

var _a, _b;
//# sourceMappingURL=allotmentteam.component.js.map

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\" *ngIf=\"hideSideBar\">\n  <router-outlet></router-outlet>\n</div>\n<div class=\"wrapper\" *ngIf=\"!hideSideBar\">\n        <div class=\"sidebar\" data-background-color=\"white\" data-active-color=\"danger\">\n            <sidebar-cmp></sidebar-cmp>\n        </div>\n        <div class=\"main-panel\">\n            <navbar-cmp></navbar-cmp>\n            <div class=\"content\">\n                <router-outlet></router-outlet>\n            </div>\n            <!-- <footer-cmp></footer-cmp> -->\n        </div>\n    </div>\n\n    <!-- <fixedplugin-cmp></fixedplugin-cmp> -->\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import * as $ from 'jquery';
var AppComponent = (function () {
    function AppComponent(router, location) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.hideSideBar = false;
        router.events.subscribe(function (val) {
            console.log(location.path());
            if (location.path() == '/login') {
                _this.hideSideBar = true;
                if (localStorage.getItem('loginData')) {
                    // this.router.navigate(['/dashboard']);
                }
            }
            else {
                if (!localStorage.getItem('loginData')) {
                    // this.router.navigate(['/login']);
                }
                _this.hideSideBar = false;
            }
            // if(location.path() != ''){
            //   this.route = location.path();
            // } else {
            //   this.route = 'Home'
            // }
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        // console.log(this.router.url);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("./src/app/app.component.html"),
        styles: [__webpack_require__("./src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["Location"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["Location"]) === "function" && _b || Object])
], AppComponent);

var _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routing__ = __webpack_require__("./src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sidebar_sidebar_module__ = __webpack_require__("./src/app/sidebar/sidebar.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_footer_footer_module__ = __webpack_require__("./src/app/shared/footer/footer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_navbar_navbar_module__ = __webpack_require__("./src/app/shared/navbar/navbar.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_fixedplugin_fixedplugin_module__ = __webpack_require__("./src/app/shared/fixedplugin/fixedplugin.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngui_map__ = __webpack_require__("./node_modules/@ngui/map/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngui_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__ngui_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_smart_table__ = __webpack_require__("./node_modules/ng2-smart-table/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__dashboard_dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__user_user_component__ = __webpack_require__("./src/app/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__dialog_dialog_component__ = __webpack_require__("./src/app/dialog/dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__leads_leads_component__ = __webpack_require__("./src/app/leads/leads.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__addlead_addlead_component__ = __webpack_require__("./src/app/addlead/addlead.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__assignedleads_assignedleads_component__ = __webpack_require__("./src/app/assignedleads/assignedleads.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__leadteam_leadteam_component__ = __webpack_require__("./src/app/leadteam/leadteam.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__allotmentteam_allotmentteam_component__ = __webpack_require__("./src/app/allotmentteam/allotmentteam.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__serviceteam_serviceteam_component__ = __webpack_require__("./src/app/serviceteam/serviceteam.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_13__dashboard_dashboard_component__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_14__user_user_component__["a" /* UserComponent */],
            __WEBPACK_IMPORTED_MODULE_15__dialog_dialog_component__["a" /* DialogComponent */],
            __WEBPACK_IMPORTED_MODULE_16__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_17__leads_leads_component__["a" /* LeadsComponent */],
            __WEBPACK_IMPORTED_MODULE_18__addlead_addlead_component__["a" /* AddleadComponent */],
            __WEBPACK_IMPORTED_MODULE_19__assignedleads_assignedleads_component__["a" /* AssignedleadsComponent */],
            __WEBPACK_IMPORTED_MODULE_20__leadteam_leadteam_component__["a" /* LeadteamComponent */],
            __WEBPACK_IMPORTED_MODULE_21__allotmentteam_allotmentteam_component__["a" /* AllotmentteamComponent */],
            __WEBPACK_IMPORTED_MODULE_22__serviceteam_serviceteam_component__["a" /* ServiceteamComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_routing__["a" /* AppRoutes */]),
            __WEBPACK_IMPORTED_MODULE_6__sidebar_sidebar_module__["a" /* SidebarModule */],
            __WEBPACK_IMPORTED_MODULE_8__shared_navbar_navbar_module__["a" /* NavbarModule */],
            __WEBPACK_IMPORTED_MODULE_7__shared_footer_footer_module__["a" /* FooterModule */],
            __WEBPACK_IMPORTED_MODULE_9__shared_fixedplugin_fixedplugin_module__["a" /* FixedPluginModule */],
            __WEBPACK_IMPORTED_MODULE_12__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_11_ng2_smart_table__["a" /* Ng2SmartTableModule */],
            __WEBPACK_IMPORTED_MODULE_10__ngui_map__["NguiMapModule"].forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE' }),
        ],
        //providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_user_component__ = __webpack_require__("./src/app/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__leads_leads_component__ = __webpack_require__("./src/app/leads/leads.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__addlead_addlead_component__ = __webpack_require__("./src/app/addlead/addlead.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assignedleads_assignedleads_component__ = __webpack_require__("./src/app/assignedleads/assignedleads.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__leadteam_leadteam_component__ = __webpack_require__("./src/app/leadteam/leadteam.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__allotmentteam_allotmentteam_component__ = __webpack_require__("./src/app/allotmentteam/allotmentteam.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__serviceteam_serviceteam_component__ = __webpack_require__("./src/app/serviceteam/serviceteam.component.ts");









var AppRoutes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__["a" /* DashboardComponent */]
    },
    {
        path: 'user',
        component: __WEBPACK_IMPORTED_MODULE_1__user_user_component__["a" /* UserComponent */]
    },
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */]
    },
    {
        path: 'leads',
        component: __WEBPACK_IMPORTED_MODULE_3__leads_leads_component__["a" /* LeadsComponent */]
    },
    {
        path: 'addlead',
        component: __WEBPACK_IMPORTED_MODULE_4__addlead_addlead_component__["a" /* AddleadComponent */]
    },
    {
        path: 'assignedleads',
        component: __WEBPACK_IMPORTED_MODULE_5__assignedleads_assignedleads_component__["a" /* AssignedleadsComponent */]
    },
    {
        path: 'leadteam',
        component: __WEBPACK_IMPORTED_MODULE_6__leadteam_leadteam_component__["a" /* LeadteamComponent */]
    },
    {
        path: 'allotmentteam',
        component: __WEBPACK_IMPORTED_MODULE_7__allotmentteam_allotmentteam_component__["a" /* AllotmentteamComponent */]
    },
    {
        path: 'serviceteam',
        component: __WEBPACK_IMPORTED_MODULE_8__serviceteam_serviceteam_component__["a" /* ServiceteamComponent */]
    }
];
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "./src/app/assignedleads/assignedleads.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/assignedleads/assignedleads.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"card\">\n              <div class=\"header\">\n                  <h4 class=\"title pull-left\">All Assigned Leads</h4>\n                  <hr>\n                  <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n              </div>\n              <div class=\"content table-responsive table-full-width \" style=\"margin-left:1px; margin-right:1px;\">\n                  <table class=\"table table-striped\">\n                      <!-- <thead>\n                          <tr>\n                              <th *ngFor=\"let cell of tableData1.headerRow\">{{ cell }}</th>\n                          </tr>\n                      </thead> -->\n                      <td>S.No.</td>\n                      <!-- <td>JobId</td> -->\n                      <td>Phone</td>\n                      <td>Category</td>\n                      <td>Brand</td>\n                      <td>Description</td>\n                      <td>Date & Time</td>\n\n                      <tbody>\n                          <tr *ngFor=\"let row of allLeadsDetail;let i=index;\">\n                              <!-- <p>{{ jobpostdetail?.length }}</p> -->\n                              <td>{{i+1}}</td>\n                              <!-- <td>{{row.job_id}}</td> -->\n                              <td>{{row.c_phone}}</td>\n                              <td>{{row.category}}</td>\n                              <td>{{row.brand}}</td>\n                              <td>{{row.description}}</td>\n                              <td>{{row.date}}</td>\n                              <!-- <td>{{row.address1}}<br>{{row.address2}}<br>{{row.city}}<br>{{row.state}}<br>{{row.zip}}</td> -->\n                              <td>\n                                <button class=\"btn-info\" style=\"color: white;\" (click)=\"viewLead(row.l_id)\" >Details</button>\n                                <!-- <button style=\"background-color: #0095FF;color: white;\" (click)=\"editLead(row.l_id)\" >Edit</button> -->\n                                <button class=\"btn-primary\" style=\"color: white;\" (click)=\"unAssignLead(row.l_id)\" >UnAssign</button>\n                            </td>\n                          </tr>\n                      </tbody>\n                  </table>\n\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n<app-dialog [(visible)]=\"showDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Lead Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n                </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                      <div class=\"col-md-12\">\n                        <p><b>Customer Phone:</b> {{customerAddressByLeadId.c_phone}}</p>\n                      </div>\n                      <div class=\"col-md-12\">\n                        <p><b>Address:</b> {{customerAddressByLeadId.name}}<br>{{customerAddressByLeadId.address}}, {{customerAddressByLeadId.city}},\n                                    {{customerAddressByLeadId.state}} - {{customerAddressByLeadId.zip}}\n                                    <br>Phone: {{customerAddressByLeadId.optional_phone}}\n                                    <br>Email: {{customerAddressByLeadId.email}}</p>\n\n                      </div>\n                      <div class=\"col-md-12\">\n                        <hr>\n                        <p><b>Category:</b> {{leadsDetailsById.category}}</p>\n                        <p><b>Brand:</b> {{leadsDetailsById.brand}}</p>\n                        <p><b>Description:</b> {{leadsDetailsById.description}}</p>\n                        <p><b>Date & Time:</b> {{leadsDetailsById.date}}</p>\n                      </div>\n                      <div class=\"col-md-12\">\n                        <hr>\n                        <p><b>Lead Alloted To:</b></p>\n                        <div class=\"col-md-6\">\n                          <p><b>Name:</b> {{service_team.name}}</p>\n                          <p><b>Email:</b> {{service_team.email}}</p>\n                          <p><b>Phone:</b> {{service_team.st_phone}}</p>\n                        </div>\n                        <div class=\"col-md-6\">\n                          <div class=\"author\">\n                            <img class=\"avatar border-white\" src=\"{{service_team.photo}}\" alt=\"...\" style=\"width: 90px; height:  90px;\" />\n                          </div>\n                        </div>\n                        <!-- <button class=\"btn btn-default\" (click)=\"assignLead()\" >Assign Lead</button> -->\n                      </div>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/assignedleads/assignedleads.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssignedleadsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AssignedleadsComponent = (function () {
    function AssignedleadsComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.leadsDetailsById = { category: null, brand: null, description: null, date: null };
        this.customerAddressByLeadId = { c_phone: null, name: null, address: null, city: null, state: null, zip: null, optional_phone: null };
        this.service_team = { name: null, email: null, st_phone: null, photo: null };
    }
    AssignedleadsComponent.prototype.ngOnInit = function () {
        this.assignedLeads();
        this.showDialog = false;
        this.showDialogUpdate = false;
    };
    AssignedleadsComponent.prototype.assignedLeads = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'getAllAssignedLeadsAdmin', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    _this.allLeadsDetail = data.data;
                    alert(data.message);
                }
                else {
                    _this.allLeadsDetail = data.data;
                    //alert(data.message);
                    //console.log(this.allLeadsDetail);
                }
            }
        });
    };
    AssignedleadsComponent.prototype.unAssignLead = function (l_id) {
        var _this = this;
        var unassign = confirm("Sure you want to unAssign the Lead?");
        if (!unassign) {
        }
        else {
            this.data = "l_id=" + l_id;
            this.httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                    'session-key': JSON.parse(localStorage.getItem('session_key'))
                })
            };
            this.http.post('http://' + this.url + 'unAssignLeadAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                    }
                    else {
                        alert(data.message);
                        _this.assignedLeads();
                    }
                    ;
                }
            });
        }
    };
    AssignedleadsComponent.prototype.viewLead = function (l_id) {
        var _this = this;
        this.l_id = l_id;
        this.data = "l_id=" + l_id;
        this.showDialog = true;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    //alert(data.message);
                    //console.log(data);
                    _this.leadsDetailsById = data.lead_data[0];
                    _this.customerAddressByLeadId = data.customer_address[0];
                    _this.getServiceTeamInfoByLeadId();
                }
                ;
            }
        });
    };
    AssignedleadsComponent.prototype.getServiceTeamInfoByLeadId = function () {
        var _this = this;
        //this.category = this.leadsDetailsById.category;
        //this.data = "category="+this.leadsDetailsById.category;
        this.data = "l_id=" + this.leadsDetailsById.l_id;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'getServiceTeamInfoByLeadIdAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    _this.service_team = data.service_team_info[0];
                    console.log(data);
                }
                ;
            }
        });
    };
    return AssignedleadsComponent;
}());
AssignedleadsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-assignedleads',
        template: __webpack_require__("./src/app/assignedleads/assignedleads.component.html"),
        styles: [__webpack_require__("./src/app/assignedleads/assignedleads.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AssignedleadsComponent);

var _a, _b;
//# sourceMappingURL=assignedleads.component.js.map

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "    <div class=\"container-fluid\">\n        <div class=\"row\">\n\n            <div class=\"col-lg-6 col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-5\">\n                                <div class=\"icon-big icon-warning text-center\">\n                                    <i class=\"ti-eye\"></i>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-7\">\n                                <div class=\"numbers\">\n                                    <p style=\"font-size: 20px;\">Total Open Leads</p>\n                                    <a *ngFor=\"let row of jobpostdetail;let i=index;\"></a>\n                                            <p style=\"font-size: 24px;\">{{ details.openLeads }}</p>\n                                </div>\n                            </div>\n                        </div>\n                        <!-- <div class=\"footer\">\n                            <hr />\n                            <div class=\"stats\">\n                                <i class=\"ti-reload\"></i> Updated now\n                            </div>\n                        </div> -->\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"col-lg-6 col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-5\">\n                                <div class=\"icon-big icon-success text-center\">\n                                    <i class=\"ti-check\"></i>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-7\">\n                                <div class=\"numbers\">\n                                    <p style=\"font-size: 20px;\">Total Assigned Leads</p>\n                                    <a *ngFor=\"let row of jobpostdetail;let i=index;\"></a>\n                                        <p style=\"font-size: 24px;\">{{ details.assignedLeads }}</p>\n                                </div>\n                            </div>\n                        </div>\n                        <!-- <div class=\"footer\">\n                            <hr />\n                            <div class=\"stats\">\n                                <i class=\"ti-calendar\"></i> Last day\n                            </div>\n                        </div> -->\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"col-lg-6 col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-5\">\n                                <div class=\"icon-big icon-success text-center\">\n                                    <i class=\"ti-user\"></i>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-7\">\n                                <div class=\"numbers\">\n                                    <p style=\"font-size: 20px;\">Lead Team</p>\n                                    <a *ngFor=\"let row of jobpostdetail;let i=index;\"></a>\n                                        <p style=\"font-size: 24px;\">{{ details.leadsTeam }}</p>\n                                </div>\n                            </div>\n                        </div>\n                        <!-- <div class=\"footer\">\n                            <hr />\n                            <div class=\"stats\">\n                                <i class=\"ti-calendar\"></i> Last day\n                            </div>\n                        </div> -->\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"col-lg-6 col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-5\">\n                                <div class=\"icon-big icon-success text-center\">\n                                    <i class=\"ti-user\"></i>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-7\">\n                                <div class=\"numbers\">\n                                    <p style=\"font-size: 20px;\">Allotment Team</p>\n                                    <a *ngFor=\"let row of jobpostdetail;let i=index;\"></a>\n                                        <p style=\"font-size: 24px;\">{{ details.allotmentTeam }}</p>\n                                </div>\n                            </div>\n                        </div>\n                        <!-- <div class=\"footer\">\n                            <hr />\n                            <div class=\"stats\">\n                                <i class=\"ti-calendar\"></i> Last day\n                            </div>\n                        </div> -->\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"col-lg-6 col-sm-6\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-5\">\n                                <div class=\"icon-big icon-success text-center\">\n                                    <i class=\"ti-user\"></i>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-7\">\n                                <div class=\"numbers\">\n                                    <p style=\"font-size: 20px;\">Service Team</p>\n                                    <a *ngFor=\"let row of jobpostdetail;let i=index;\"></a>\n                                        <p style=\"font-size: 24px;\">{{ details.serviceTeam }}</p>\n                                </div>\n                            </div>\n                        </div>\n                        <!-- <div class=\"footer\">\n                            <hr />\n                            <div class=\"stats\">\n                                <i class=\"ti-calendar\"></i> Last day\n                            </div>\n                        </div> -->\n                    </div>\n                </div>\n            </div>\n\n        </div>\n\n\n    </div>\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DashboardComponent = (function () {
    function DashboardComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.details = { openLeads: null, assignedLeads: null };
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.getInfoForDashboard();
        //   let win = (window as any);
        // if(!sessionStorage.getItem('reload')) {
        //     sessionStorage.setItem('reload','reload')
        //     win.location.reload();
        // }
    };
    // JOB POST Count
    DashboardComponent.prototype.getInfoForDashboard = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'adminDashboard', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                _this.details = data;
                console.log(_this.details);
            }
        });
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'dashboard-cmp',
        template: __webpack_require__("./src/app/dashboard/dashboard.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], DashboardComponent);

var _a, _b;
//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "./src/app/dialog/dialog.component.css":
/***/ (function(module, exports) {

module.exports = ".overlay {\r\n    position: fixed;\r\n    top: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    right: 0;\r\n    background-color: rgba(0, 0, 0, 0.5);\r\n    z-index: 999;\r\n  }\r\n  \r\n  .dialog {\r\n    z-index: 1000;\r\n    position: fixed;\r\n    right: 0;\r\n    left: 0;\r\n    top: 20px;\r\n    margin-right: auto;\r\n    margin-left: auto;\r\n    min-height: 200px;\r\n    width: 90%;\r\n    max-width: 520px;\r\n    background-color: #fff;\r\n    padding: 12px;\r\n    -webkit-box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2), 0 13px 19px 2px rgba(0, 0, 0, 0.14), 0 5px 24px 4px rgba(0, 0, 0, 0.12);\r\n            box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2), 0 13px 19px 2px rgba(0, 0, 0, 0.14), 0 5px 24px 4px rgba(0, 0, 0, 0.12);\r\n  }\r\n  \r\n  @media (min-width: 768px) {\r\n    .dialog {\r\n      top: 40px;\r\n    }\r\n  }\r\n  \r\n  .dialog__close-btn {\r\n    border: 0;\r\n    background: none;\r\n    color: #2d2d2d;\r\n    position: absolute;\r\n    top: 8px;\r\n    right: 8px;\r\n    font-size: 1.2em;\r\n  }"

/***/ }),

/***/ "./src/app/dialog/dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div [@dialog] *ngIf=\"visible\" class=\"dialog\">\n    <ng-content></ng-content>\n    <button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button>\n  </div>\n  <div *ngIf=\"visible\" class=\"overlay\" (click)=\"close()\"></div>"

/***/ }),

/***/ "./src/app/dialog/dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__("./node_modules/@angular/animations/@angular/animations.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DialogComponent = (function () {
    function DialogComponent() {
        this.closable = true;
        this.visibleChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    DialogComponent.prototype.ngOnInit = function () { };
    DialogComponent.prototype.close = function () {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    return DialogComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], DialogComponent.prototype, "closable", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], DialogComponent.prototype, "visible", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], DialogComponent.prototype, "visibleChange", void 0);
DialogComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-dialog',
        template: __webpack_require__("./src/app/dialog/dialog.component.html"),
        styles: [__webpack_require__("./src/app/dialog/dialog.component.css")],
        animations: [
            Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* trigger */])('dialog', [
                Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["h" /* transition */])('void => *', [
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["g" /* style */])({ transform: 'scale3d(.3, .3, .3)' }),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])(100)
                ]),
                Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["h" /* transition */])('* => void', [
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])(100, Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["g" /* style */])({ transform: 'scale3d(.0, .0, .0)' }))
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [])
], DialogComponent);

var _a;
//# sourceMappingURL=dialog.component.js.map

/***/ }),

/***/ "./src/app/leads/leads.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/leads/leads.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"card\">\n              <div class=\"header\">\n                  <h4 class=\"title pull-left\">All Open Leads</h4>\n                  <hr>\n                  <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n              </div>\n              <div class=\"content table-responsive table-full-width \" style=\"margin-left:1px; margin-right:1px;\">\n                  <table class=\"table table-striped\">\n                      <!-- <thead>\n                          <tr>\n                              <th *ngFor=\"let cell of tableData1.headerRow\">{{ cell }}</th>\n                          </tr>\n                      </thead> -->\n                      <td>S.No.</td>\n                      <!-- <td>JobId</td> -->\n                      <td>Phone</td>\n                      <td>Category</td>\n                      <td>Brand</td>\n                      <td>Description</td>\n                      <td>Date & Time</td>\n\n                      <tbody>\n                          <tr *ngFor=\"let row of allLeadsDetail;let i=index;\">\n                              <!-- <p>{{ jobpostdetail?.length }}</p> -->\n                              <td>{{i+1}}</td>\n                              <!-- <td>{{row.job_id}}</td> -->\n                              <td>{{row.c_phone}}</td>\n                              <td>{{row.category}}</td>\n                              <td>{{row.brand}}</td>\n                              <td>{{row.description}}</td>\n                              <td>{{row.date}}</td>\n                              <!-- <td>{{row.address1}}<br>{{row.address2}}<br>{{row.city}}<br>{{row.state}}<br>{{row.zip}}</td> -->\n                              <td>\n                                <button class=\"btn-primary\" style=\"color: white;\" (click)=\"viewLead(row.l_id)\" >Assign Lead</button>\n                                <button class=\"btn-info\" style=\"color: white;\" (click)=\"editLead(row.l_id)\" >Edit</button>\n                                <button class=\"btn-danger\" style=\"color: white;\" (click)=\"deleteLead(row.l_id)\" >Delete</button>\n                            </td>\n                          </tr>\n                      </tbody>\n                  </table>\n\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n<app-dialog [(visible)]=\"showDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Lead Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n                </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                      <div class=\"col-md-12\">\n                        <p><b>Customer Phone:</b> {{customerAddressByLeadId.c_phone}}</p>\n                      </div>\n                      <div class=\"col-md-12\">\n                        <p><b>Address:</b> {{customerAddressByLeadId.name}}<br>{{customerAddressByLeadId.address}}, {{customerAddressByLeadId.city}},\n                                    {{customerAddressByLeadId.state}} - {{customerAddressByLeadId.zip}}\n                                    <br>Phone:{{customerAddressByLeadId.optional_phone}}\n                                    <br>Email:{{customerAddressByLeadId.email}}</p>\n                      </div>\n                      <div class=\"col-md-12\">\n                        <hr>\n                        <p><b>Category:</b> {{leadsDetailsById.category}}</p>\n                        <p><b>Brand:</b> {{leadsDetailsById.brand}}</p>\n                        <p><b>Description:</b> {{leadsDetailsById.description}}</p>\n                        <p><b>Date & Time:</b> {{leadsDetailsById.date}}</p>\n                      </div>\n                      <div class=\"col-md-12\">\n                        <hr>\n                        <p><b>Select Service Team:</b>\n                          <select [(ngModel)]=\"selected_st_phone\" class=\"form-control border-input\">\n                            <option *ngFor=\"let s of service_team\" [ngValue]=\"s.st_phone\">{{s.st_phone}} ~ ({{s.name}})</option>\n                          </select>\n                        </p>\n                        <button class=\"btn btn-default\" (click)=\"assignLead()\" >Assign Lead</button>\n                      </div>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showDialogEdit\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Edit Lead Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n                </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                      <div class=\"col-md-12\">\n                        <p><b>Customer Phone:</b> {{customerAddressByLeadId.c_phone}}</p>\n                      </div>\n                      <div class=\"col-md-12\">\n                        <p><b>Address:</b> {{customerAddressByLeadId.name}}<br>{{customerAddressByLeadId.address}}, {{customerAddressByLeadId.city}},\n                                    {{customerAddressByLeadId.state}} - {{customerAddressByLeadId.zip}}\n                                    <br>Phone: {{customerAddressByLeadId.optional_phone}}\n                                    <br>Email:{{customerAddressByLeadId.email}}</p>\n                      </div>\n\n                      <div class=\"col-md-6 form-group\">\n                        <hr>\n                        <p><b>Select Category: </b></p>\n                        <select [(ngModel)]=\"selectedCategory\" class=\"form-control border-input\">\n                          <option *ngFor=\"let c of category\" [ngValue]=\"c\">{{c}}</option>\n                        </select>\n                      </div>\n                      <div class=\"col-md-6 form-group\">\n                        <hr>\n                        <p><b>Select Brand: </b></p>\n                        <select [(ngModel)]=\"selectedBrand\" class=\"form-control border-input\">\n                          <option *ngFor=\"let b of brand;let i=index;\" [ngValue]=\"b\">{{b}}</option>\n                        </select>\n                      </div>\n                      <div class=\"col-md-12 form-group\">\n                        <textarea class=\"form-control border-input\" rows=\"5\" [(ngModel)]=\"description\"></textarea>\n                      </div>\n\n                      <div class=\"col-md-12\">\n                        <button class=\"btn btn-default\" (click)=\"updateLeadDetailsById()\" >Update</button>\n                      </div>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/leads/leads.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeadsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// declare interface TableData {
//     headerRow: string[];
//     dataRows: string[][];
// }
var LeadsComponent = (function () {
    function LeadsComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.leadsDetailsById = { category: null, brand: null, description: null, date: null };
        this.customerAddressByLeadId = { c_phone: null, name: null, address: null, city: null, state: null, zip: null, optional_phone: null };
        this.category = ['AC', 'TV', 'WASHING-MACHINE', 'REFRIGERATOR', 'CHIMNEY', 'WATER-PURIFIER', 'MICROWAVE-OVEN', 'GEYSER'];
        this.brand = ['LG', 'SONY', 'SAMSUNG', 'IFB', 'WHIRLPOOL', 'BOSCH', 'SIEMENS', 'VIDEOCON', 'ONIDA'];
    }
    //constructor(public router: Router){ }
    LeadsComponent.prototype.ngOnInit = function () {
        this.allLeads();
        this.showDialog = false;
        this.showDialogUpdate = false;
    };
    LeadsComponent.prototype.allLeads = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'getAllOpenLeadsAdmin', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    _this.allLeadsDetail = data.data;
                    //alert(data.message);
                    //console.log(this.allLeadsDetail);
                }
            }
        });
    };
    LeadsComponent.prototype.deleteLead = function (l_id) {
        var _this = this;
        var del = confirm("Sure you want to Delete the Lead?");
        if (!del) {
            //do nothing
        }
        else {
            this.data = "l_id=" + l_id;
            this.httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                    'session-key': JSON.parse(localStorage.getItem('session_key'))
                })
            };
            this.http.post('http://' + this.url + 'deleteOpenLeadByIdAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                    }
                    else {
                        alert(data.message);
                        _this.allLeads();
                    }
                    ;
                }
            });
        }
    };
    LeadsComponent.prototype.viewLead = function (l_id) {
        var _this = this;
        this.l_id = l_id;
        this.data = "l_id=" + l_id;
        this.showDialog = true;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    //alert(data.message);
                    //console.log(data);
                    _this.leadsDetailsById = data.lead_data[0];
                    _this.customerAddressByLeadId = data.customer_address[0];
                    _this.getServiceTeamByCategory();
                }
                ;
            }
        });
    };
    LeadsComponent.prototype.getServiceTeamByCategory = function () {
        var _this = this;
        //this.category = this.leadsDetailsById.category;
        this.data = "category=" + this.leadsDetailsById.category;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'getServiceTeamByCategoryAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    _this.service_team = null;
                    _this.selected_st_phone = null;
                    alert(data.message);
                }
                else {
                    //alert(data.message);
                    _this.service_team = null;
                    _this.selected_st_phone = null;
                    console.log(data);
                    _this.service_team = data.service_team;
                    //this.showDialogUpdate = true;
                }
                ;
            }
        });
    };
    LeadsComponent.prototype.assignLead = function () {
        var _this = this;
        this.data = "l_id=" + this.l_id + "&c_phone=" + this.customerAddressByLeadId.c_phone + "&st_phone=" + this.selected_st_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'assignLeadAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    alert(data.message);
                    _this.sendMessage();
                    _this.showDialog = false;
                    _this.allLeads();
                }
                ;
            }
        });
    };
    LeadsComponent.prototype.editLead = function (l_id) {
        var _this = this;
        this.l_id = l_id;
        this.data = "l_id=" + l_id;
        this.showDialogEdit = true;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    _this.leadsDetailsById = data.lead_data[0];
                    _this.customerAddressByLeadId = data.customer_address[0];
                    _this.selectedCategory = _this.leadsDetailsById.category;
                    _this.selectedBrand = _this.leadsDetailsById.brand;
                    _this.description = _this.leadsDetailsById.description;
                }
                ;
            }
        });
    };
    LeadsComponent.prototype.updateLeadDetailsById = function () {
        var _this = this;
        this.data = 'l_id=' + this.leadsDetailsById.l_id + '&category=' + this.selectedCategory + '&brand=' + this.selectedBrand + '&description=' + this.description;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'updateLeadDetailsById', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    alert(data.message);
                    _this.selectedCategory = '';
                    _this.selectedBrand = '';
                    _this.description = '';
                    _this.showDialogEdit = false;
                    _this.allLeads();
                }
            }
        });
    };
    LeadsComponent.prototype.sendMessage = function () {
        this.data = "l_id=" + this.l_id + "&st_phone=" + this.selected_st_phone + "&ca_id=" + this.customerAddressByLeadId.ca_id;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
            })
        };
        this.http.post('http://' + this.url + 'send_message', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.error) {
                alert(data.message);
            }
            else {
                alert(data.message);
            }
        });
    };
    return LeadsComponent;
}());
LeadsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-leads',
        template: __webpack_require__("./src/app/leads/leads.component.html"),
        styles: [__webpack_require__("./src/app/leads/leads.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], LeadsComponent);

var _a, _b;
//# sourceMappingURL=leads.component.js.map

/***/ }),

/***/ "./src/app/leadteam/leadteam.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/leadteam/leadteam.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"card\">\n              <div class=\"header\">\n                  <h4 class=\"title pull-left\">Leads Team</h4>\n                  <button class=\"btn-success\" style=\"float: right; color: white;\" (click)=\"showAddDialog()\" >Add New Member</button>\n                  <hr>\n                  <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n              </div>\n              <div class=\"content table-responsive table-full-width \" style=\"margin-left:1px; margin-right:1px;\">\n                  <table class=\"table table-striped\">\n                      <!-- <thead>\n                          <tr>\n                              <th *ngFor=\"let cell of tableData1.headerRow\">{{ cell }}</th>\n                          </tr>\n                      </thead> -->\n                      <td>S.No.</td>\n                      <!-- <td>JobId</td> -->\n                      <td>Phone</td>\n                      <td>Name</td>\n                      <td>Email</td>\n\n                      <tbody>\n                          <tr *ngFor=\"let row of allLeadsTeam;let i=index;\">\n                              <!-- <p>{{ jobpostdetail?.length }}</p> -->\n                              <td>{{i+1}}</td>\n                              <!-- <td>{{row.job_id}}</td> -->\n                              <td>{{row.lt_phone}}</td>\n                              <td>{{row.name}}</td>\n                              <td>{{row.email}}</td>\n                              <!-- <td>{{row.address1}}<br>{{row.address2}}<br>{{row.city}}<br>{{row.state}}<br>{{row.zip}}</td> -->\n                              <td>\n                                <button class=\"btn-info\" style=\"color: white;\" (click)=\"viewLeadTeamDetails(row.lt_phone)\" >Details</button>\n                                <button class=\"btn-primary\" style=\"color: white;\" (click)=\"editLeadTeamDetails(row.lt_phone)\" >Edit</button>\n                                <!-- <button style=\"background-color: #0095FF;color: white;\" (click)=\"editLead(row.l_id)\" >Edit</button> -->\n                                <button class=\"btn-danger\" style=\"color: white;\" (click)=\"deleteLeadTeam(row.lt_phone)\" >Remove</button>\n                            </td>\n                          </tr>\n                      </tbody>\n                  </table>\n\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n<app-dialog [(visible)]=\"showDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Lead Team Member Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                      <div class=\"col-md-3\">\n                        <div class=\"author\">\n                          <img class=\"avatar border-white\" src=\"{{lt_details.photo}}\" alt=\"...\" style=\"width: 100px; height:  100px;\" />\n                        </div>\n                      </div>\n                      <div class=\"col-md-9\">\n                        <p><b>Phone:</b> {{lt_details.lt_phone}}</p>\n                        <p><b>Name:</b> {{lt_details.name}}</p>\n                        <p><b>Address:</b> {{lt_details.address}}, {{lt_details.city}},\n                                    {{lt_details.state}} - {{lt_details.zip}}</p>\n                        <p><b>Password:</b> {{lt_details.password}}</p>\n                      </div>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showEditDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Update Lead Team Member Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                    <div class=\"col-md-3 form-group\">\n                      <div class=\"author\">\n                        <img class=\"avatar border-white\" src=\"{{lt_details.photo}}\" alt=\"...\" style=\"width: 90px; height:  90px;\" />\n                      </div>\n                    </div>\n                    <div class=\"col-md-9 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.lt_phone\" name=\"lt_phone\"  type=\"number\" placeholder=\"Enter Phone Number\" readonly>\n                    </div>\n                    <div class=\"col-md-9 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.name\" name=\"lt_name\" type=\"text\" placeholder=\"Enter Name\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.email\" name=\"lt_email\" type=\"email\" placeholder=\"Enter Email\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.aadhaar\" name=\"lt_aadhaar\" type=\"text\" placeholder=\"Enter Aadhaar\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <textarea class=\"form-control border-input\" [(ngModel)]=\"lt_details.address\" name=\"lt_address\" type=\"text\" rows=\"7\" placeholder=\"Enter Address\"></textarea>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.city\" name=\"lt_city\" type=\"text\" placeholder=\"Enter City\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.state\" name=\"lt_state\" type=\"text\" placeholder=\"Enter State\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.zip\" name=\"lt_zip\" type=\"number\" placeholder=\"Enter Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_details.password\" name=\"lt_password\" type=\"text\" placeholder=\"Enter Password\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" type=\"submit\" value=\"Submit\" (click)=\"updateLeadTeamMember()\">Update Details</button>\n                    </div>\n\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showAddNewDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Add New Lead Team Member</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                   <form>\n                    <div class=\"col-md-12 form-group\">\n                      <input type=\"file\" placeholder=\"Choose Image\" name=\"image\" class=\"col-md-12\" (change)=\"fileChanged($event)\" #ProfilePic>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_phone\" name=\"lt_phone\"  type=\"number\" placeholder=\"Enter Phone Number\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_name\" name=\"lt_name\" type=\"text\" placeholder=\"Enter Name\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_email\" name=\"lt_email\" type=\"email\" placeholder=\"Enter Email\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_aadhaar\" name=\"lt_aadhaar\" type=\"text\" placeholder=\"Enter Aadhaar\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <textarea class=\"form-control border-input\" [(ngModel)]=\"lt_address\" name=\"lt_address\" type=\"text\" rows=\"8\" placeholder=\"Enter Address\"></textarea>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_city\" name=\"lt_city\" type=\"text\" placeholder=\"Enter City\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_state\" name=\"lt_state\" type=\"text\" placeholder=\"Enter State\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_zip\" name=\"lt_zip\" type=\"number\" placeholder=\"Enter Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"lt_password\" name=\"lt_password\" type=\"text\" placeholder=\"Enter Password\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" type=\"submit\" value=\"Submit\" (click)=\"addNewLeadTeam()\">Add Member</button>\n                    </div>\n                  </form>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/leadteam/leadteam.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeadteamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LeadteamComponent = (function () {
    function LeadteamComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.lt_details = { lt_phone: null, name: null, address: null, city: null, state: null, zip: null, aadhaar: null, photo: null };
        this.lt_phone = null;
    }
    LeadteamComponent.prototype.ngOnInit = function () {
        this.leadsTeam();
    };
    LeadteamComponent.prototype.leadsTeam = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'leadsTeam', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.allLeadsTeam = null;
                }
                else {
                    _this.allLeadsTeam = data.leadsteam;
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    LeadteamComponent.prototype.viewLeadTeamDetails = function (lt_phone) {
        var _this = this;
        this.showDialog = true;
        this.data = "lt_phone=" + lt_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'viewLeadTeamDetailsByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.lt_details = null;
                }
                else {
                    _this.lt_details = data.lt_details[0];
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    LeadteamComponent.prototype.deleteLeadTeam = function (lt_phone) {
        var _this = this;
        var check = confirm('Sure You want to Remove the Lead Team Member?');
        if (!check) {
            //do nothing
        }
        else {
            this.data = "lt_phone=" + lt_phone;
            this.httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                    'session-key': JSON.parse(localStorage.getItem('session_key'))
                })
            };
            this.http.post('http://' + this.url + 'deleteLeadTeamByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                    }
                    else {
                        alert(data.message);
                        _this.leadsTeam();
                    }
                }
            });
        }
    };
    LeadteamComponent.prototype.editLeadTeamDetails = function (lt_phone) {
        var _this = this;
        this.showEditDialog = true;
        this.data = "lt_phone=" + lt_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'viewLeadTeamDetailsByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.lt_details = null;
                }
                else {
                    _this.lt_details = data.lt_details[0];
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    LeadteamComponent.prototype.showAddDialog = function () {
        this.showAddNewDialog = true;
    };
    LeadteamComponent.prototype.addNewLeadTeam = function () {
        var _this = this;
        var fileList = this.image.target.files;
        console.log(fileList);
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData = new FormData();
            formData.append('image', file, file.name);
            formData.append('lt_phone', this.lt_phone);
            formData.append('lt_name', this.lt_name);
            formData.append('lt_email', this.lt_email);
            formData.append('lt_aadhaar', this.lt_aadhaar);
            formData.append('lt_address', this.lt_address);
            formData.append('lt_city', this.lt_city);
            formData.append('lt_state', this.lt_state);
            formData.append('lt_zip', this.lt_zip);
            formData.append('lt_password', this.lt_password);
            //   this.httpOptions = {
            //     headers : new Headers({
            //     'Content-Type':  'application/json',
            //     'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            //     'session-key': JSON.parse(localStorage.getItem('session_key'))
            //   })
            // };
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Accept': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            });
            //headers.append('Accept', 'application/json');
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            this.http.post('http://' + this.url + 'addNewLeadTeam', formData, options).map(function (res) { return res.json(); }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].throw(error); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                        //this.lt_details = null;
                    }
                    else {
                        alert(data.message);
                        _this.lt_name = null;
                        _this.lt_phone = null;
                        _this.lt_email = null;
                        _this.lt_aadhaar = null;
                        _this.lt_address = null;
                        _this.lt_city = null;
                        _this.lt_state = null;
                        _this.lt_zip = null;
                        _this.image = null;
                        _this.lt_password = null;
                        _this.myInputVariable.nativeElement.value = "";
                        _this.leadsTeam();
                        _this.showAddNewDialog = false;
                    }
                }
            }, function (error) { return console.log(error); });
        }
        else {
            alert('Choose Image');
        }
    };
    LeadteamComponent.prototype.fileChanged = function (event) {
        this.image = event;
        console.log(this.image);
    };
    LeadteamComponent.prototype.updateLeadTeamMember = function () {
        var _this = this;
        //lt_password = this.lt_password;
        this.data = this.lt_details, { lt_password: this.lt_password };
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'updateLeadTeamMember', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.lt_details = null;
                }
                else {
                    //this.lt_details=data.lt_details[0];
                    alert(data.message);
                    _this.showEditDialog = false;
                    //console.log(this.c_address);
                }
            }
        });
    };
    return LeadteamComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ProfilePic'),
    __metadata("design:type", Object)
], LeadteamComponent.prototype, "myInputVariable", void 0);
LeadteamComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-leadteam',
        template: __webpack_require__("./src/app/leadteam/leadteam.component.html"),
        styles: [__webpack_require__("./src/app/leadteam/leadteam.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]) === "function" && _b || Object])
], LeadteamComponent);

var _a, _b;
//# sourceMappingURL=leadteam.component.js.map

/***/ }),

/***/ "./src/app/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"\" style=\"text-align:center;background:url(assets/img/backimage.jpg);background-size:100%;display:inline-block;width:100%;height:100%;\">\n\n    <form (ngSubmit)=\"doLogin()\" class=\"card col-sm-offset-4 col-sm-4 col-xs-12\" style=\"display: inline-block;padding: 20px;top: 20vh;\">\n      <div class=\"\">\n        <img src=\"./assets/img/GoodService.jpg\" alt=\"\" style=\"width:100px;\">\n         <h4 style=\"margin-top:0;\">Good Service ~ Admin</h4>\n      </div>\n      <div class=\"\">\n        <input type=\"tel\" name=\"\" value=\"\" [(ngModel)]=\"phone\" name=\"phone\" placeholder=\"Enter Phone Number\" style=\"width:100%;margin-top:20px;padding:10px;\">\n      </div>\n      <div class=\"\">\n        <input type=\"password\" name=\"\" value=\"\" [(ngModel)]=\"password\" name=\"password\"  placeholder=\"Enter Password\" style=\"width:100%;margin-top:20px;padding:10px;\">\n      </div>\n      <div class=\"\">\n        <input type=\"submit\" value=\"Login\" style=\"width:100%;margin-top:20px;padding:10px;background-color: rgba(84, 84, 230, 0.44);\">\n      </div>\n    </form>\n\n  </div>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.checkSession();
    };
    LoginComponent.prototype.checkSession = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'checkSessionAdmin', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.error) {
                _this.router.navigate(['/login']);
            }
            else {
                _this.router.navigate(['/dashboard']);
                //window.location.reload();
            }
        });
    };
    LoginComponent.prototype.doLogin = function () {
        var _this = this;
        this.data = "user_phone=" + this.phone + '&user_pass=' + this.password;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        };
        this.http.post('http://' + this.url + 'adminLogin', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.error) {
                alert(data.message);
            }
            else {
                localStorage.setItem('session_key', JSON.stringify(data.session_key));
                localStorage.setItem('user_phone', _this.phone);
                _this.router.navigate(['/dashboard']);
            }
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__("./src/app/login/login.component.html"),
        styles: [__webpack_require__("./src/app/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "./src/app/serviceteam/serviceteam.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/serviceteam/serviceteam.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n      <div class=\"col-md-12\">\n          <div class=\"card\">\n              <div class=\"header\">\n                  <h4 class=\"title pull-left\">Service Team</h4>\n                  <button class=\"btn-success\" style=\"float: right; color: white;\" (click)=\"showAddDialog()\" >Add New Member</button>\n                  <hr>\n                  <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n              </div>\n              <div class=\"content table-responsive table-full-width \" style=\"margin-left:1px; margin-right:1px;\">\n                  <table class=\"table table-striped\">\n                      <!-- <thead>\n                          <tr>\n                              <th *ngFor=\"let cell of tableData1.headerRow\">{{ cell }}</th>\n                          </tr>\n                      </thead> -->\n                      <td>S.No.</td>\n                      <!-- <td>JobId</td> -->\n                      <td>Phone</td>\n                      <td>Name</td>\n                      <td>Email</td>\n\n                      <tbody>\n                          <tr *ngFor=\"let row of allAllotmentTeam;let i=index;\">\n                              <!-- <p>{{ jobpostdetail?.length }}</p> -->\n                              <td>{{i+1}}</td>\n                              <!-- <td>{{row.job_id}}</td> -->\n                              <td>{{row.st_phone}}</td>\n                              <td>{{row.name}}</td>\n                              <td>{{row.email}}</td>\n                              <!-- <td>{{row.address1}}<br>{{row.address2}}<br>{{row.city}}<br>{{row.state}}<br>{{row.zip}}</td> -->\n                              <td>\n                                <button class=\"btn-info\" style=\"color: white;\" (click)=\"viewServiceTeamDetails(row.st_phone)\" >Details</button>\n                                <button class=\"btn-primary\" style=\"color: white;\" (click)=\"editServiceTeamDetails(row.st_phone)\" >Edit</button>\n                                <!-- <button style=\"background-color: #0095FF;color: white;\" (click)=\"editLead(row.l_id)\" >Edit</button> -->\n                                <button class=\"btn-danger\" style=\"color: white;\" (click)=\"deleteServiceTeam(row.st_phone)\" >Remove</button>\n                            </td>\n                          </tr>\n                      </tbody>\n                  </table>\n\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n<app-dialog [(visible)]=\"showDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Service Team Member Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                      <div class=\"col-md-3\">\n                        <div class=\"author\">\n                          <img class=\"avatar border-white\" src=\"{{st_details.photo}}\" alt=\"...\" style=\"width: 100px; height:  100px;\" />\n                        </div>\n                      </div>\n                      <div class=\"col-md-9\">\n                        <p><b>Phone:</b> {{st_details.st_phone}}</p>\n                        <p><b>Name:</b> {{st_details.name}}</p>\n                        <p><b>Address:</b> {{st_details.address}}, {{st_details.city}},\n                                    {{st_details.state}} - {{st_details.zip}}</p>\n                        <p><b>Password:</b> {{st_details.password}}</p>\n                        <p><b>Category:</b></p>\n                        <ul *ngFor='let cat of category; let i = index'>\n                            <div class=\"col-md-12\">\n                              <li *ngIf='categ[cat]==1'>{{cat}}</li>\n                            </div>\n                          </ul>\n                      </div>\n                  </div>\n                  <!-- <div class=\"col-md-12\" *ngFor='let cat of category'>\n                    <div >\n                      {{(cat==1)?cat:''}}\n\n                    </div>\n\n                  </div> -->\n                  <!-- <div class=\"col-md-12\" *ngFor='let cat of category; let i = index'>\n                    {{categ.AC}}\n                  </div> -->\n\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showEditDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Update Service Team Member Details</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                    <div class=\"col-md-3 form-group\">\n                      <div class=\"author\">\n                        <img class=\"avatar border-white\" src=\"{{st_details.photo}}\" alt=\"...\" style=\"width: 90px; height:  90px;\" />\n                      </div>\n                    </div>\n                    <div class=\"col-md-9 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.st_phone\" name=\"st_phone\"  type=\"number\" placeholder=\"Enter Phone Number\" readonly>\n                    </div>\n                    <div class=\"col-md-9 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.name\" name=\"st_name\" type=\"text\" placeholder=\"Enter Name\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.email\" name=\"st_email\" type=\"email\" placeholder=\"Enter Email\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.aadhaar\" name=\"st_aadhaar\" type=\"text\" placeholder=\"Enter Aadhaar\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <textarea class=\"form-control border-input\" [(ngModel)]=\"st_details.address\" name=\"st_address\" type=\"text\" rows=\"7\" placeholder=\"Enter Address\"></textarea>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.city\" name=\"st_city\" type=\"text\" placeholder=\"Enter City\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.state\" name=\"st_state\" type=\"text\" placeholder=\"Enter State\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.zip\" name=\"st_zip\" type=\"number\" placeholder=\"Enter Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_details.password\" name=\"st_password\" type=\"text\" placeholder=\"Enter Password\">\n                    </div>\n                    <div class=\"col-md-12 form-group\" id=\"check\" >\n                      <p>Select Category:</p>\n                      <div *ngFor='let cat of category; let i = index'>\n                        <div class=\"col-md-6\">\n                        <label>\n                          <input [checked]=categ[cat] type=\"checkbox\" name=\"category\" value=\"{{cat}}\"\n                                 (change)=\"updateCheckedCategory(cat, $event)\" #abc>\n                          {{cat}}\n                        </label>\n                        </div>\n                    </div>\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" type=\"submit\" value=\"Submit\" (click)=\"updateServiceTeamMember()\">Update Details</button>\n                    </div>\n\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n\n<app-dialog [(visible)]=\"showAddNewDialog\">\n  <div class=\"container-fluid \">\n    <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <div class=\"card\">\n                <div class=\"header\">\n                    <h4 class=\"title pull-left\">Add New Service Team Member</h4>\n                    <hr>\n                    <!-- <div class=\"pull-right create-button\" (click)=\"showDialog = !showDialog\">Create New Job</div> -->\n               </div>\n                <div class=\"content\">\n                  <div class=\"row\">\n                   <form>\n                    <div class=\"col-md-12 form-group\">\n                      <input accept=\"image/*\" type=\"file\" placeholder=\"Choose Image\" name=\"image\" class=\"col-md-12\" (change)=\"fileChanged($event)\" #ProfilePic>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_phone\" name=\"st_phone\"  type=\"number\" placeholder=\"Enter Phone Number\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_name\" name=\"st_name\" type=\"text\" placeholder=\"Enter Name\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_email\" name=\"st_email\" type=\"email\" placeholder=\"Enter Email\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_aadhaar\" name=\"st_aadhaar\" type=\"text\" placeholder=\"Enter Aadhaar\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <textarea class=\"form-control border-input\" [(ngModel)]=\"st_address\" name=\"st_address\" type=\"text\" rows=\"8\" placeholder=\"Enter Address\"></textarea>\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_city\" name=\"st_city\" type=\"text\" placeholder=\"Enter City\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_state\" name=\"st_state\" type=\"text\" placeholder=\"Enter State\">\n                    </div>\n                    <div class=\"col-md-6 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_zip\" name=\"st_zip\" type=\"number\" placeholder=\"Enter Zip\">\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                      <input class=\"form-control border-input\" [(ngModel)]=\"st_password\" name=\"st_password\" type=\"text\" placeholder=\"Enter Password\">\n                    </div>\n                    <div class=\"col-md-12 form-group\" id=\"check\" >\n                      <p>Select Category:</p>\n                      <div *ngFor='let cat of category; let i = index'>\n                        <div class=\"col-md-6\">\n                        <label>\n                          <input [checked]='categoryMap.cat' type=\"checkbox\" name=\"category\" value=\"{{cat}}\"\n                                 (change)=\"updateCheckedCategory(cat, $event)\" #abc>\n                          {{cat}}\n                        </label>\n                        </div>\n                    </div>\n                    </div>\n                    <div class=\"col-md-12 form-group\">\n                        <button class=\"btn btn-success\" type=\"submit\" value=\"Submit\" (click)=\"addNewServiceTeam()\">Add Member</button>\n                    </div>\n                  </form>\n                  </div>\n                </div>\n            </div>\n        </div>\n      </div>\n    </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/serviceteam/serviceteam.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceteamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ServiceteamComponent = (function () {
    function ServiceteamComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.st_details = { st_phone: null, name: null, address: null, city: null, state: null, zip: null, aadhaar: null, photo: null };
        this.st_phone = null;
        this.category = ['AC', 'TV', 'WASHING-MACHINE', 'REFRIGERATOR', 'CHIMNEY', 'WATER-PURIFIER', 'MICROWAVE-OVEN', 'GEYSER'];
        this.categoryMap = {
            AC: false,
            TV: false,
            'WASHING-MACHINE': false,
            REFRIGERATOR: false,
            CHIMNEY: false,
            'WATER-PURIFIER': false,
            'MICROWAVE-OVEN': false,
            GEYSER: false,
        };
        this.categoryChecked = [];
        this.selected = false;
        //categ:any;
        this.categ = { AC: 0, TV: 0, 'WASHING-MACHINE': 0, REFRIGERATOR: 0, CHIMNEY: 0, 'WATER-PURIFIER': 0, 'MICROWAVE-OVEN': 0, GEYSER: 0 };
    }
    ServiceteamComponent.prototype.initCategoryMap = function () {
        for (var x = 0; x < this.category.length; x++) {
            this.categoryMap[this.category[x]] = true;
        }
    };
    ServiceteamComponent.prototype.updateCheckedCategory = function (cat, event) {
        this.categoryMap[cat] = event.target.checked;
        this.updateOptions();
    };
    ServiceteamComponent.prototype.updateOptions = function () {
        this.categoryChecked = [];
        for (var x in this.categoryMap) {
            if (this.categoryMap[x]) {
                this.categoryChecked.push(x);
            }
        }
        //this.categ = this.categoryChecked;
        //this.categoryChecked = [];
        console.log(this.categoryChecked);
    };
    ServiceteamComponent.prototype.ngOnInit = function () {
        this.serviceTeamMembers();
    };
    ServiceteamComponent.prototype.serviceTeamMembers = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'serviceTeamMembers', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.allAllotmentTeam = null;
                }
                else {
                    _this.allAllotmentTeam = data.serviceteam;
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    ServiceteamComponent.prototype.viewServiceTeamDetails = function (st_phone) {
        var _this = this;
        this.showDialog = true;
        this.data = "st_phone=" + st_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'viewServiceTeamDetailsByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.st_details = null;
                }
                else {
                    _this.st_details = data.st_details[0];
                    _this.categ = data.st_category[0];
                    //alert(data.message);
                    console.log(_this.categ);
                }
            }
        });
    };
    ServiceteamComponent.prototype.deleteServiceTeam = function (st_phone) {
        var _this = this;
        var check = confirm('Sure You want to Remove the Service Team Member?');
        if (!check) {
            //do nothing
        }
        else {
            this.data = "st_phone=" + st_phone;
            this.httpOptions = {
                headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                    'session-key': JSON.parse(localStorage.getItem('session_key'))
                })
            };
            this.http.post('http://' + this.url + 'deleteServiceTeamByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                    }
                    else {
                        alert(data.message);
                        _this.serviceTeamMembers();
                    }
                }
            });
        }
    };
    ServiceteamComponent.prototype.editServiceTeamDetails = function (st_phone) {
        var _this = this;
        this.showEditDialog = true;
        this.data = "st_phone=" + st_phone;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'viewServiceTeamDetailsByPhone', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    _this.st_details = null;
                }
                else {
                    _this.st_details = data.st_details[0];
                    _this.categ = data.st_category[0];
                    console.log(_this.categ);
                    for (var i = 0; i < _this.category.length; i++) {
                        _this.categoryMap[_this.category[i]] = _this.categ[_this.category[i]];
                    }
                    _this.updateOptions();
                    console.log(_this.categoryMap);
                    //console.log(this.categ['AC']);
                    //alert(data.message);
                    //console.log(this.c_address);
                }
            }
        });
    };
    ServiceteamComponent.prototype.showAddDialog = function () {
        //document.getElementById('check').innerHTML.reload(true);
        //this.myInput.nativeElement.checked=false;
        //console.log(this.myInput.nativeElement);
        // this.categoryMap.AC = false;
        // this.router.navigate(['blank']);
        // console.log(this.categoryMap.AC);
        this.showAddNewDialog = true;
        this.selected = false;
    };
    ServiceteamComponent.prototype.addNewServiceTeam = function () {
        var _this = this;
        var fileList = this.image.target.files;
        console.log(fileList);
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData = new FormData();
            formData.append('image', file, file.name);
            formData.append('st_phone', this.st_phone);
            formData.append('st_name', this.st_name);
            formData.append('st_email', this.st_email);
            formData.append('st_aadhaar', this.st_aadhaar);
            formData.append('st_address', this.st_address);
            formData.append('st_city', this.st_city);
            formData.append('st_state', this.st_state);
            formData.append('st_zip', this.st_zip);
            formData.append('st_password', this.st_password);
            formData.append('category', this.categoryChecked);
            //   this.httpOptions = {
            //     headers : new Headers({
            //     'Content-Type':  'application/json',
            //     'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            //     'session-key': JSON.parse(localStorage.getItem('session_key'))
            //   })
            // };
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Accept': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            });
            //headers.append('Accept', 'application/json');
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            this.http.post('http://' + this.url + 'addNewServiceTeam', formData, options).map(function (res) { return res.json(); }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].throw(error); }).subscribe(function (data) {
                if (data.sessionExpire) {
                    _this.router.navigate(['/login']);
                    alert(data.message);
                }
                else {
                    if (data.error) {
                        alert(data.message);
                        //this.st_details = null;
                    }
                    else {
                        alert(data.message);
                        _this.st_name = null;
                        _this.st_phone = null;
                        _this.st_email = null;
                        _this.st_aadhaar = null;
                        _this.st_address = null;
                        _this.st_city = null;
                        _this.st_state = null;
                        _this.st_zip = null;
                        _this.image = null;
                        _this.st_password = null;
                        // this.category.every(function(item:any) {
                        //   return item.selected == false;
                        // })
                        _this.selected = false;
                        _this.myInputVariable.nativeElement.value = "";
                        _this.serviceTeamMembers();
                        _this.showAddNewDialog = false;
                    }
                }
            }, function (error) { return console.log(error); });
        }
        else {
            alert('Choose Image');
        }
    };
    ServiceteamComponent.prototype.fileChanged = function (event) {
        this.image = event;
        console.log(this.image);
    };
    ServiceteamComponent.prototype.updateServiceTeamMember = function () {
        var _this = this;
        //st_password = this.st_password;
        this.data = { st_details: this.st_details, category: this.categoryChecked };
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.post('http://' + this.url + 'updateServiceTeamMember', this.data, this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                    //this.st_details = null;
                }
                else {
                    //this.st_details=data.st_details[0];
                    alert(data.message);
                    _this.showEditDialog = false;
                    _this.serviceTeamMembers();
                    //console.log(this.c_address);
                }
            }
        });
    };
    return ServiceteamComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ProfilePic'),
    __metadata("design:type", Object)
], ServiceteamComponent.prototype, "myInputVariable", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('abc'),
    __metadata("design:type", Object)
], ServiceteamComponent.prototype, "myInput", void 0);
ServiceteamComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-serviceteam',
        template: __webpack_require__("./src/app/serviceteam/serviceteam.component.html"),
        styles: [__webpack_require__("./src/app/serviceteam/serviceteam.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]) === "function" && _b || Object])
], ServiceteamComponent);

var _a, _b;
//# sourceMappingURL=serviceteam.component.js.map

/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"fixed-plugin\">\n    <div class=\"dropdown show-dropdown\">\n        <a href=\"#\" data-toggle=\"dropdown\">\n        <i class=\"fa fa-cog fa-2x\"> </i>\n        </a>\n        <ul class=\"dropdown-menu\">\n            <li class=\"header-title\">Sidebar Background</li>\n            <li class=\"adjustments-line text-center\">\n                <a href=\"javascript:void(0)\" class=\"switch-trigger background-color\">\n                        <span class=\"badge filter badge-white active\" data-color=\"white\"></span>\n                        <span class=\"badge filter badge-black\" data-color=\"black\"></span>\n                </a>\n            </li>\n\n\t\t\t<li class=\"header-title\">Sidebar Active Color</li>\n            <li class=\"adjustments-line text-center\">\n                <a href=\"javascript:void(0)\" class=\"switch-trigger active-color\">\n                        <span class=\"badge filter badge-primary\" data-color=\"primary\"></span>\n                        <span class=\"badge filter badge-info\" data-color=\"info\"></span>\n                        <span class=\"badge filter badge-success\" data-color=\"success\"></span>\n                        <span class=\"badge filter badge-warning\" data-color=\"warning\"></span>\n                        <span class=\"badge filter badge-danger active\" data-color=\"danger\"></span>\n                </a>\n            </li>\n\n        </ul>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixedPluginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FixedPluginComponent = (function () {
    function FixedPluginComponent() {
    }
    FixedPluginComponent.prototype.ngOnInit = function () {
        var $sidebar = $('.sidebar');
        var $off_canvas_sidebar = $('.off-canvas-sidebar');
        var window_width = $(window).width();
        if (window_width > 767) {
            if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                $('.fixed-plugin .dropdown').addClass('open');
            }
        }
        $('.fixed-plugin a').click(function (event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });
        $('.fixed-plugin .background-color span').click(function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('color');
            if ($sidebar.length != 0) {
                $sidebar.attr('data-background-color', new_color);
            }
            if ($off_canvas_sidebar.length != 0) {
                $off_canvas_sidebar.attr('data-background-color', new_color);
            }
        });
        $('.fixed-plugin .active-color span').click(function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('color');
            if ($sidebar.length != 0) {
                $sidebar.attr('data-active-color', new_color);
            }
            if ($off_canvas_sidebar.length != 0) {
                $off_canvas_sidebar.attr('data-active-color', new_color);
            }
        });
    };
    return FixedPluginComponent;
}());
FixedPluginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'fixedplugin-cmp',
        template: __webpack_require__("./src/app/shared/fixedplugin/fixedplugin.component.html")
    })
], FixedPluginComponent);

//# sourceMappingURL=fixedplugin.component.js.map

/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixedPluginModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fixedplugin_component__ = __webpack_require__("./src/app/shared/fixedplugin/fixedplugin.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FixedPluginModule = (function () {
    function FixedPluginModule() {
    }
    return FixedPluginModule;
}());
FixedPluginModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__fixedplugin_component__["a" /* FixedPluginComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_3__fixedplugin_component__["a" /* FixedPluginComponent */]]
    })
], FixedPluginModule);

//# sourceMappingURL=fixedplugin.module.js.map

/***/ }),

/***/ "./src/app/shared/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\n    <div class=\"container-fluid\">\n        <nav class=\"pull-left\">\n           \n        </nav>\n        <div class=\"copyright pull-right\">\n           \n        </div>\n    </div>\n</footer>\n"

/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    return FooterComponent;
}());
FooterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'footer-cmp',
        template: __webpack_require__("./src/app/shared/footer/footer.component.html")
    })
], FooterComponent);

//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "./src/app/shared/footer/footer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__footer_component__ = __webpack_require__("./src/app/shared/footer/footer.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FooterModule = (function () {
    function FooterModule() {
    }
    return FooterModule;
}());
FooterModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__footer_component__["a" /* FooterComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_3__footer_component__["a" /* FooterComponent */]]
    })
], FooterModule);

//# sourceMappingURL=footer.module.js.map

/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default\">\n        <div class=\"container-fluid\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" (click)=\"sidebarToggle()\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar bar1\"></span>\n                    <span class=\"icon-bar bar2\"></span>\n                    <span class=\"icon-bar bar3\"></span>\n                </button>\n                <p class=\"navbar-brand\" >{{getTitle()}}</p>\n            </div>\n            <div class=\"collapse navbar-collapse\">\n                <ul class=\"nav navbar-nav navbar-right\" style=\"top: 20px;position: relative;\">\n                  <li (click)=\"logout()\" style=\"font-weight: 600;font-size: 20px;\">\n                <i class=\"ti-logout\"></i>\n                <p>Logout</p>\n                  </li>\n                </ul>\n\n            </div>\n        </div>\n    </nav>\n"

/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__ = __webpack_require__("./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = (function () {
    function NavbarComponent(location, renderer, element, router) {
        this.renderer = renderer;
        this.element = element;
        this.router = router;
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.loginPath = 'login';
        this.listTitles = __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__["a" /* ROUTES */].filter(function (listTitle) { return listTitle; });
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    };
    NavbarComponent.prototype.getTitle = function () {
        var titlee = window.location.pathname;
        titlee = titlee.substring(1);
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return 'Dashboard';
    };
    NavbarComponent.prototype.sidebarToggle = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        }
        else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    };
    NavbarComponent.prototype.logout = function () {
        localStorage.clear();
        sessionStorage.removeItem('reload');
        this.router.navigate(['/login']);
    };
    return NavbarComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("navbar-cmp"),
    __metadata("design:type", Object)
], NavbarComponent.prototype, "button", void 0);
NavbarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'navbar-cmp',
        template: __webpack_require__("./src/app/shared/navbar/navbar.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _d || Object])
], NavbarComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ "./src/app/shared/navbar/navbar.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__navbar_component__ = __webpack_require__("./src/app/shared/navbar/navbar.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NavbarModule = (function () {
    function NavbarModule() {
    }
    return NavbarModule;
}());
NavbarModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__navbar_component__["a" /* NavbarComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_3__navbar_component__["a" /* NavbarComponent */]]
    })
], NavbarModule);

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar-wrapper\">\n        <div class=\"logo\">\n            <a class=\"simple-text\">\n                <div class=\"logo-img\">\n                    <img src=\"./assets/img/GoodService.jpg\" alt=\"\">\n                </div>\n                Good Service <br>ADMIN\n            </a>\n        </div>\n        <ul class=\"nav\">\n                <!-- <li *ngIf=\"isNotMobileMenu()\">\n                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                        <i class=\"ti-panel\"></i>\n                        <p>Stats</p>\n                    </a>\n                </li>\n                <li class=\"dropdown\" *ngIf=\"isNotMobileMenu()\">\n                      <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                            <i class=\"ti-bell\"></i>\n                            <p class=\"notification\">5</p>\n                            <p>Notifications</p>\n                            <b class=\"caret\"></b>\n                      </a>\n                      <ul class=\"dropdown-menu\">\n                        <li><a href=\"#\">Notification 1</a></li>\n                        <li><a href=\"#\">Notification 2</a></li>\n                        <li><a href=\"#\">Notification 3</a></li>\n                        <li><a href=\"#\">Notification 4</a></li>\n                        <li><a href=\"#\">Another notification</a></li>\n                      </ul>\n                </li>\n                <li *ngIf=\"isNotMobileMenu()\">\n                    <a href=\"#\">\n                        <i class=\"ti-settings\"></i>\n                        <p>Settings</p>\n                    </a>\n                </li> -->\n                <!-- <li class=\"divider\" *ngIf=\"isNotMobileMenu()\"></li> -->\n            <li *ngFor=\"let menuItem of menuItems\" routerLinkActive=\"active\" class=\"{{menuItem.class}}\">\n                <a [routerLink]=\"[menuItem.path]\">\n                    <i class=\"{{menuItem.icon}}\"></i>\n                    <p>{{menuItem.title}}</p>\n                </a>\n            </li>\n            <li *ngIf=\"isNotMobileMenu()\" (click)=\"logout()\">\n                <a href=\"#\">\n                <i class=\"ti-settings\"></i>\n                <p>Logout</p>\n                </a>\n            </li>\n        </ul>\n    </div>\n"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ROUTES = [
    { path: 'dashboard', title: 'Dashboard', icon: 'ti-panel', class: '' },
    //{ path: 'jobposting', title: 'JobPost',  icon:'ti-layout-media-overlay-alt', class: '' },
    //{ path: 'employer', title: 'Employer',  icon:'ti-id-badge', class: '' },
    //{ path: 'applicant', title: 'Applicant',  icon:'ti-user', class: '' },
    //{ path: 'payment', title: 'Payment',  icon:'ti-layout-cta-center', class: '' },
    // { path: 'login', title: 'Login',  icon:'ti-text', class: '' },
    //{ path: 'user', title: 'User Profile',  icon:'ti-user', class: '' },
    // { path: 'table', title: 'Table List',  icon:'ti-view-list-alt', class: '' },
    { path: 'leads', title: 'Open Leads', icon: 'ti-eye', class: '' },
    { path: 'assignedleads', title: 'Assigned Leads', icon: 'ti-check', class: '' },
    { path: 'addlead', title: 'Add Lead', icon: 'ti-plus', class: '' },
    { path: 'leadteam', title: 'Leads Team', icon: 'ti-user', class: '' },
    { path: 'allotmentteam', title: 'Allotment Team', icon: 'ti-user', class: '' },
    { path: 'serviceteam', title: 'Service Team', icon: 'ti-user', class: '' },
];
var SidebarComponent = (function () {
    function SidebarComponent(router) {
        this.router = router;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
    };
    SidebarComponent.prototype.isNotMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    SidebarComponent.prototype.logout = function () {
        localStorage.clear();
        sessionStorage.removeItem('reload');
        this.router.navigate(['/login']);
    };
    return SidebarComponent;
}());
SidebarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sidebar-cmp',
        template: __webpack_require__("./src/app/sidebar/sidebar.component.html"),
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object])
], SidebarComponent);

var _a;
//# sourceMappingURL=sidebar.component.js.map

/***/ }),

/***/ "./src/app/sidebar/sidebar.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sidebar_component__ = __webpack_require__("./src/app/sidebar/sidebar.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SidebarModule = (function () {
    function SidebarModule() {
    }
    return SidebarModule;
}());
SidebarModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__sidebar_component__["b" /* SidebarComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_3__sidebar_component__["b" /* SidebarComponent */]]
    })
], SidebarModule);

//# sourceMappingURL=sidebar.module.js.map

/***/ }),

/***/ "./src/app/user/user.component.html":
/***/ (function(module, exports) {

module.exports = "    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-lg-4 col-md-5\">\n                <div class=\"card card-user\">\n                    <div class=\"image\">\n                        <img src=\"assets/img/background.jpg\" alt=\"...\"/>\n                    </div>\n                    <div class=\"content\">\n                        <div class=\"author\">\n                          <img class=\"avatar border-white\" src=\"{{allotmentTeamProfile.photo}}\" alt=\" \"/>\n                          <h4 class=\"title\">{{allotmentTeamProfile.name}}<br /></h4>\n                        </div>\n                        <div class=\"email\">\n                          <p><label>Email: </label> {{allotmentTeamProfile.email}}</p>\n                        </div>\n                        <div class=\"phone\">\n                          <p><label>Phone: </label> {{user_phone_number}}</p>\n                        </div>\n                        <div class=\"aadhaar\">\n                          <p><label>Aadhaar: </label> {{allotmentTeamProfile.aadhaar}}</p>\n                        </div>\n                        <div class=\"address\">\n                          <p><label>Address: </label>\n                            {{allotmentTeamProfile.address}},\n                            {{allotmentTeamProfile.city}},\n                            {{allotmentTeamProfile.state}} -\n                            {{allotmentTeamProfile.zip}},\n                          </p>\n                        </div>\n                    </div>\n\n                </div>\n\n            </div>\n            <!-- <div class=\"col-lg-4 col-md-5\">\n                <div class=\"card card-user\">\n                    <div class=\"image\">\n                        <img src=\"assets/img/background.jpg\" alt=\"...\"/>\n                    </div>\n                    <div class=\"content\">\n                        <div class=\"author\">\n                          <img class=\"avatar border-white\" src=\"{{leadTeamProfile.photo}}\" alt=\"...\"/>\n                          <h4 class=\"title\">{{leadTeamProfile.name}}<br /></h4>\n                        </div>\n                        <div class=\"email\">\n                          <p><label>Email: </label> {{leadTeamProfile.email}}</p>\n                        </div>\n                        <div class=\"phone\">\n                          <p><label>Phone: </label> {{user_phone_number}}</p>\n                        </div>\n                        <div class=\"aadhaar\">\n                          <p><label>Aadhaar: </label> {{leadTeamProfile.aadhaar}}</p>\n                        </div>\n                    </div>\n\n                </div>\n\n            </div> -->\n            <!-- <div class=\"col-lg-8 col-md-7\">\n                <div class=\"card\">\n                    <div class=\"header\">\n                        <h4 class=\"title\">Edit Profile</h4>\n                    </div>\n                    <div class=\"content\">\n                        <form>\n                            <div class=\"row\">\n                                <div class=\"col-md-5\">\n                                    <div class=\"form-group\">\n                                        <label>Company</label>\n                                        <input type=\"text\" class=\"form-control border-input\" disabled placeholder=\"Company\" value=\"Creative Code Inc.\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-3\">\n                                    <div class=\"form-group\">\n                                        <label>Username</label>\n                                        <input type=\"text\" class=\"form-control border-input\" placeholder=\"Username\" value=\"michael23\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-4\">\n                                    <div class=\"form-group\">\n                                        <label for=\"exampleInputEmail1\">Email address</label>\n                                        <input type=\"email\" class=\"form-control border-input\" placeholder=\"Email\">\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"row\">\n                                <div class=\"col-md-6\">\n                                    <div class=\"form-group\">\n                                        <label>First Name</label>\n                                        <input type=\"text\" class=\"form-control border-input\" placeholder=\"Company\" value=\"Chet\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class=\"form-group\">\n                                        <label>Last Name</label>\n                                        <input type=\"text\" class=\"form-control border-input\" placeholder=\"Last Name\" value=\"Faker\">\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"row\">\n                                <div class=\"col-md-12\">\n                                    <div class=\"form-group\">\n                                        <label>Address</label>\n                                        <input type=\"text\" class=\"form-control border-input\" placeholder=\"Home Address\" value=\"Melbourne, Australia\">\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"row\">\n                                <div class=\"col-md-4\">\n                                    <div class=\"form-group\">\n                                        <label>City</label>\n                                        <input type=\"text\" class=\"form-control border-input\" placeholder=\"City\" value=\"Melbourne\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-4\">\n                                    <div class=\"form-group\">\n                                        <label>Country</label>\n                                        <input type=\"text\" class=\"form-control border-input\" placeholder=\"Country\" value=\"Australia\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-4\">\n                                    <div class=\"form-group\">\n                                        <label>Postal Code</label>\n                                        <input type=\"number\" class=\"form-control border-input\" placeholder=\"ZIP Code\">\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"row\">\n                                <div class=\"col-md-12\">\n                                    <div class=\"form-group\">\n                                        <label>About Me</label>\n                                        <textarea rows=\"5\" class=\"form-control border-input\" placeholder=\"Here can be your description\" value=\"Mike\">\nOh so, your weak rhyme\nYou doubt I'll bother, reading into it\nI'll probably won't, left to my own devices\nBut that's the difference in our opinions.</textarea>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"text-center\">\n                                <button type=\"submit\" class=\"btn btn-info btn-fill btn-wd\">Update Profile</button>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                        </form>\n                    </div>\n                </div>\n            </div> -->\n\n\n        </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserComponent = (function () {
    function UserComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {};
        this.url = 'softbizz.in/goodservice/api/public/';
        this.user_phone_number = JSON.parse(localStorage.getItem('user_phone'));
        this.allotmentTeamProfile = { name: null, email: null, aadhar: null, address: null, city: null, state: null, zip: null, photo: null };
    }
    UserComponent.prototype.ngOnInit = function () {
        this.allotmentTeamProfileInfo();
    };
    UserComponent.prototype.allotmentTeamProfileInfo = function () {
        var _this = this;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                'session-key': JSON.parse(localStorage.getItem('session_key'))
            })
        };
        this.http.get('http://' + this.url + 'allotmentTeamProfileInfo', this.httpOptions).map(function (res) { return res.json(); }).subscribe(function (data) {
            if (data.sessionExpire) {
                _this.router.navigate(['/login']);
                alert(data.message);
            }
            else {
                if (data.error) {
                    alert(data.message);
                }
                else {
                    _this.allotmentTeamProfile = data.data[0];
                    //alert(data.message);
                    //console.log(this.allLeadsDetail);
                }
            }
        });
    };
    return UserComponent;
}());
UserComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'user-cmp',
        template: __webpack_require__("./src/app/user/user.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], UserComponent);

var _a, _b;
//# sourceMappingURL=user.component.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map