import { Component, OnInit, ViewChild } from '@angular/core';
import { Jsonp } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-serviceteam',
  templateUrl: './serviceteam.component.html',
  styleUrls: ['./serviceteam.component.css']
})
export class ServiceteamComponent implements OnInit {

  model:any={};
  httpOptions : any;
  posts:any;
  data:any;
  url : string = 'softbizz.in/goodservice/api/public/';
  allAllotmentTeam:any;
  st_details:any={st_phone:null, name:null, address:null, city:null, state:null, zip:null, aadhaar:null, photo:null};
  showDialog : boolean;
  st_phone:any = null;
  showEditDialog:boolean;
  showAddNewDialog:boolean;
  st_name:any;
  st_email:any;
  st_aadhaar:any;
  st_address:any;
  st_city:any;
  st_state:any;
  st_zip:any;
  image:any;
  st_password:any;
  category = ['AC','TV','WASHING-MACHINE','REFRIGERATOR','CHIMNEY','WATER-PURIFIER','MICROWAVE-OVEN','GEYSER'];
  categoryMap = {
    AC: false,
    TV: false,
    'WASHING-MACHINE': false,
    REFRIGERATOR: false,
    CHIMNEY: false,
    'WATER-PURIFIER': false,
    'MICROWAVE-OVEN': false,
    GEYSER: false,
  };
  categoryChecked:any = [];
  selected:boolean = false;
  //categ:any;
  categ:any = {AC: 0, TV: 0, 'WASHING-MACHINE': 0, REFRIGERATOR: 0, CHIMNEY: 0, 'WATER-PURIFIER': 0, 'MICROWAVE-OVEN': 0, GEYSER: 0};
  initCategoryMap() {
    for (var x = 0; x<this.category.length; x++) {
        this.categoryMap[this.category[x]] = true;
    }
  }

  updateCheckedCategory(cat, event) {
   this.categoryMap[cat] = event.target.checked;
   this.updateOptions();
  }

  updateOptions() {
    this.categoryChecked = [];
    for(var x in this.categoryMap) {
        if(this.categoryMap[x]) {
            this.categoryChecked.push(x);
        }
    }
    //this.categ = this.categoryChecked;
    //this.categoryChecked = [];
    console.log(this.categoryChecked);
  }

  @ViewChild('ProfilePic') myInputVariable: any;
  @ViewChild('abc') myInput: any;
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    this.serviceTeamMembers();
  }

  public serviceTeamMembers(){
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'serviceTeamMembers',  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.allAllotmentTeam = null;
        }
        else{
          this.allAllotmentTeam=data.serviceteam;
          //alert(data.message);
          //console.log(this.c_address);
        }
      }

    });
  }

  public viewServiceTeamDetails(st_phone){
    this.showDialog = true;
    this.data = "st_phone="+st_phone;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'viewServiceTeamDetailsByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.st_details = null;
        }
        else{
          this.st_details=data.st_details[0];
          this.categ = data.st_category[0];
          //alert(data.message);
          console.log(this.categ);
        }
      }

    });
  }

  public deleteServiceTeam(st_phone){
    var check = confirm('Sure You want to Remove the Service Team Member?');
    if(!check){
      //do nothing
    }
    else{
      this.data = "st_phone="+st_phone;
      this.httpOptions = {
        headers : new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'deleteServiceTeamByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            alert(data.message);
            this.serviceTeamMembers();
          }
        }

      });
    }
  }

  public editServiceTeamDetails(st_phone){
    this.showEditDialog = true;
    this.data = "st_phone="+st_phone;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'viewServiceTeamDetailsByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.st_details = null;
        }
        else{
          this.st_details=data.st_details[0];
          this.categ = data.st_category[0];
          console.log(this.categ);
          for(let i=0; i<this.category.length; i++){
            this.categoryMap[this.category[i]]=this.categ[this.category[i]];
          }
          this.updateOptions();

          console.log(this.categoryMap);
          //console.log(this.categ['AC']);
          //alert(data.message);
          //console.log(this.c_address);
        }
      }
    });
  }

  public showAddDialog(){
    //document.getElementById('check').innerHTML.reload(true);

    //this.myInput.nativeElement.checked=false;
    //console.log(this.myInput.nativeElement);
    // this.categoryMap.AC = false;
    // this.router.navigate(['blank']);
    // console.log(this.categoryMap.AC);
    this.showAddNewDialog = true;
    this.selected = false;

  }

  public addNewServiceTeam(){
    let fileList: FileList = this.image.target.files;
    console.log(fileList);
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('image', file, file.name);
        formData.append('st_phone',this.st_phone);
        formData.append('st_name',this.st_name);
        formData.append('st_email',this.st_email);
        formData.append('st_aadhaar',this.st_aadhaar);
        formData.append('st_address',this.st_address);
        formData.append('st_city',this.st_city);
        formData.append('st_state',this.st_state);
        formData.append('st_zip',this.st_zip);
        formData.append('st_password',this.st_password);
        formData.append('category',this.categoryChecked);


      //   this.httpOptions = {
      //     headers : new Headers({
      //     'Content-Type':  'application/json',
      //     'user-phone': JSON.parse(localStorage.getItem('user_phone')),
      //     'session-key': JSON.parse(localStorage.getItem('session_key')),
      //'role': localStorage.getItem('role')
      //   })
      // };
        let headers = new Headers({
        'Accept':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      });
        //headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        this.http.post('http://'+this.url+'addNewServiceTeam', formData, options).map(res => res.json()).catch(error => Observable.throw(error)).subscribe(
                data => {
                  if(data.sessionExpire){
                      this.router.navigate(['/login']);
                      alert(data.message);
                    }
                  else{
                    if(data.error){
                      alert(data.message);
                      //this.st_details = null;
                    }
                    else{
                      alert(data.message);
                      this.st_name=null;
                      this.st_phone=null;
                      this.st_email=null;
                      this.st_aadhaar=null;
                      this.st_address=null;
                      this.st_city=null;
                      this.st_state=null;
                      this.st_zip=null;
                      this.image=null;
                      this.st_password=null;
                      // this.category.every(function(item:any) {
                      //   return item.selected == false;
                      // })
                      this.selected = false;
                      this.myInputVariable.nativeElement.value = "";
                      this.serviceTeamMembers();
                      this.showAddNewDialog = false;
                    }
                  }

                },
                error => console.log(error)
            )
      }
      else{
        alert('Choose Image');
      }
  }

  public fileChanged(event){
    this.image = event;
     console.log(this.image);
  }

  public updateServiceTeamMember(){
    //st_password = this.st_password;
    this.data = {st_details:this.st_details,category:this.categoryChecked};
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'updateServiceTeamMember', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          //this.st_details = null;
        }
        else{
          //this.st_details=data.st_details[0];
          alert(data.message);
          this.showEditDialog = false;
          this.serviceTeamMembers();
          //console.log(this.c_address);
        }
      }
    });
  }

}
