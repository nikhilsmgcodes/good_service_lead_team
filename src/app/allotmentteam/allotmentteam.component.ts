import { Component, OnInit, ViewChild } from '@angular/core';
import { Jsonp } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-allotmentteam',
  templateUrl: './allotmentteam.component.html',
  styleUrls: ['./allotmentteam.component.css']
})
export class AllotmentteamComponent implements OnInit {
  model:any={};
  httpOptions : any;
  posts:any;
  data:any;
  url : string = 'softbizz.in/goodservice/api/public/';
  allAllotmentTeam:any;
  at_details:any={at_phone:null, name:null, address:null, city:null, state:null, zip:null, aadhaar:null, photo:null};
  showDialog : boolean;
  at_phone:any = null;
  showEditDialog:boolean;
  showAddNewDialog:boolean;
  at_name:any;
  at_email:any;
  at_aadhaar:any;
  at_address:any;
  at_city:any;
  at_state:any;
  at_zip:any;
  image:any;
  at_password:any;
  @ViewChild('ProfilePic') myInputVariable: any;
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    this.allotmentTeamMembers();
  }

  public allotmentTeamMembers(){
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'allotmentTeamMembers',  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.allAllotmentTeam = null;
        }
        else{
          this.allAllotmentTeam=data.allotmentteam;
          //alert(data.message);
          //console.log(this.c_address);
        }
      }

    });
  }

  public viewAllotmentTeamDetails(at_phone){
    this.showDialog = true;
    this.data = "at_phone="+at_phone;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'viewAllotmentTeamDetailsByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.at_details = null;
        }
        else{
          this.at_details=data.at_details[0];
          //alert(data.message);
          //console.log(this.c_address);
        }
      }

    });
  }

  public deleteAllotmentTeam(at_phone){
    var check = confirm('Sure You want to Remove the Allotment Team Member?');
    if(!check){
      //do nothing
    }
    else{
      this.data = "at_phone="+at_phone;
      this.httpOptions = {
        headers : new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'deleteAllotmentTeamByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            alert(data.message);
            this.allotmentTeamMembers();
          }
        }

      });
    }
  }

  public editAllotmentTeamDetails(at_phone){
    this.showEditDialog = true;
    this.data = "at_phone="+at_phone;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'viewAllotmentTeamDetailsByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.at_details = null;
        }
        else{
          this.at_details=data.at_details[0];
          //alert(data.message);
          //console.log(this.c_address);
        }
      }
    });
  }

  public showAddDialog(){
    this.showAddNewDialog = true;
  }

  public addNewAllotmentTeam(){
    let fileList: FileList = this.image.target.files;
    console.log(fileList);
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('image', file, file.name);
        formData.append('at_phone',this.at_phone);
        formData.append('at_name',this.at_name);
        formData.append('at_email',this.at_email);
        formData.append('at_aadhaar',this.at_aadhaar);
        formData.append('at_address',this.at_address);
        formData.append('at_city',this.at_city);
        formData.append('at_state',this.at_state);
        formData.append('at_zip',this.at_zip);
        formData.append('at_password',this.at_password);

      //   this.httpOptions = {
      //     headers : new Headers({
      //     'Content-Type':  'application/json',
      //     'user-phone': JSON.parse(localStorage.getItem('user_phone')),
      //     'session-key': JSON.parse(localStorage.getItem('session_key')),
      //'role': localStorage.getItem('role')
      //   })
      // };
        let headers = new Headers({
        'Accept':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      });
        //headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        this.http.post('http://'+this.url+'addNewAllotmentTeam', formData, options).map(res => res.json()).catch(error => Observable.throw(error)).subscribe(
                data => {
                  if(data.sessionExpire){
                      this.router.navigate(['/login']);
                      alert(data.message);
                    }
                  else{
                    if(data.error){
                      alert(data.message);
                      //this.at_details = null;
                    }
                    else{
                      alert(data.message);
                      this.at_name=null;
                      this.at_phone=null;
                      this.at_email=null;
                      this.at_aadhaar=null;
                      this.at_address=null;
                      this.at_city=null;
                      this.at_state=null;
                      this.at_zip=null;
                      this.image=null;
                      this.at_password=null;
                      this.myInputVariable.nativeElement.value = "";
                      this.allotmentTeamMembers();
                      this.showAddNewDialog = false;
                    }
                  }

                },
                error => console.log(error)
            )
      }
      else{
        alert('Choose Image');
      }
  }

  public fileChanged(event){
    this.image = event;
     console.log(this.image);
  }

  public updateAllotmentTeamMember(){
    //at_password = this.at_password;
    this.data = this.at_details,{at_password:this.at_password};
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'updateAllotmentTeamMember', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.at_details = null;
        }
        else{
          //this.at_details=data.at_details[0];
          alert(data.message);
          this.showEditDialog = false;
          //console.log(this.c_address);
        }
      }
    });
  }
}
