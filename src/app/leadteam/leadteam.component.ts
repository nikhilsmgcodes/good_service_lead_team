import { Component, OnInit, ViewChild } from '@angular/core';
import { Jsonp } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leadteam',
  templateUrl: './leadteam.component.html',
  styleUrls: ['./leadteam.component.css']
})
export class LeadteamComponent implements OnInit {
  model:any={};
  httpOptions : any;
  posts:any;
  data:any;
  url : string = 'softbizz.in/goodservice/api/public/';
  allLeadsTeam:any;
  lt_details:any={lt_phone:null, name:null, address:null, city:null, state:null, zip:null, aadhaar:null, photo:null};
  showDialog : boolean;
  lt_phone:any = null;
  showEditDialog:boolean;
  showAddNewDialog:boolean;
  lt_name:any;
  lt_email:any;
  lt_aadhaar:any;
  lt_address:any;
  lt_city:any;
  lt_state:any;
  lt_zip:any;
  image:any;
  lt_password:any;
  @ViewChild('ProfilePic') myInputVariable: any;
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    this.leadsTeam();
  }

  public leadsTeam(){
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'leadsTeam',  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.allLeadsTeam = null;
        }
        else{
          this.allLeadsTeam=data.leadsteam;
          //alert(data.message);
          //console.log(this.c_address);
        }
      }

    });
  }

  public viewLeadTeamDetails(lt_phone){
    this.showDialog = true;
    this.data = "lt_phone="+lt_phone;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'viewLeadTeamDetailsByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.lt_details = null;
        }
        else{
          this.lt_details=data.lt_details[0];
          //alert(data.message);
          //console.log(this.c_address);
        }
      }

    });
  }

  public deleteLeadTeam(lt_phone){
    var check = confirm('Sure You want to Remove the Lead Team Member?');
    if(!check){
      //do nothing
    }
    else{
      this.data = "lt_phone="+lt_phone;
      this.httpOptions = {
        headers : new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'deleteLeadTeamByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            alert(data.message);
            this.leadsTeam();
          }
        }

      });
    }
  }

  public editLeadTeamDetails(lt_phone){
    this.showEditDialog = true;
    this.data = "lt_phone="+lt_phone;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'viewLeadTeamDetailsByPhone', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.lt_details = null;
        }
        else{
          this.lt_details=data.lt_details[0];
          //alert(data.message);
          //console.log(this.c_address);
        }
      }
    });
  }

  public showAddDialog(){
    this.showAddNewDialog = true;
  }

  public addNewLeadTeam(){
    let fileList: FileList = this.image.target.files;
    console.log(fileList);
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('image', file, file.name);
        formData.append('lt_phone',this.lt_phone);
        formData.append('lt_name',this.lt_name);
        formData.append('lt_email',this.lt_email);
        formData.append('lt_aadhaar',this.lt_aadhaar);
        formData.append('lt_address',this.lt_address);
        formData.append('lt_city',this.lt_city);
        formData.append('lt_state',this.lt_state);
        formData.append('lt_zip',this.lt_zip);
        formData.append('lt_password',this.lt_password);

      //   this.httpOptions = {
      //     headers : new Headers({
      //     'Content-Type':  'application/json',
      //     'user-phone': JSON.parse(localStorage.getItem('user_phone')),
      //     'session-key': JSON.parse(localStorage.getItem('session_key')),
      //'role': localStorage.getItem('role')
      //   })
      // };
        let headers = new Headers({
        'Accept':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      });
        //headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        this.http.post('http://'+this.url+'addNewLeadTeam', formData, options).map(res => res.json()).catch(error => Observable.throw(error)).subscribe(
                data => {
                  if(data.sessionExpire){
                      this.router.navigate(['/login']);
                      alert(data.message);
                    }
                  else{
                    if(data.error){
                      alert(data.message);
                      //this.lt_details = null;
                    }
                    else{
                      alert(data.message);
                      this.lt_name=null;
                      this.lt_phone=null;
                      this.lt_email=null;
                      this.lt_aadhaar=null;
                      this.lt_address=null;
                      this.lt_city=null;
                      this.lt_state=null;
                      this.lt_zip=null;
                      this.image=null;
                      this.lt_password=null;
                      this.myInputVariable.nativeElement.value = "";
                      this.leadsTeam();
                      this.showAddNewDialog = false;
                    }
                  }

                },
                error => console.log(error)
            )
      }
      else{
        alert('Choose Image');
      }
  }

  public fileChanged(event){
    this.image = event;
     console.log(this.image);
  }

  public updateLeadTeamMember(){
    //lt_password = this.lt_password;
    this.data = this.lt_details;
    this.httpOptions = {
      headers : new Headers({
        'Content-Type':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'updateLeadTeamMember', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          //this.lt_details = null;
        }
        else{
          //this.lt_details=data.lt_details[0];
          alert(data.message);
          this.showEditDialog = false;
          //console.log(this.c_address);
        }
      }
    });
  }
}
