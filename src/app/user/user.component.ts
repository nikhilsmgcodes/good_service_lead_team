import { Component, OnInit } from '@angular/core';
import { Jsonp } from '@angular/http';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit{
  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  user_phone_number : any = JSON.parse(localStorage.getItem('user_phone'));
  allotmentTeamProfile : any = {name:null,email:null,aadhar:null,address:null,city:null,state:null,zip:null,photo:null}

  constructor(public http: Http, public router: Router) { }
    ngOnInit(){
      this.allotmentTeamProfileInfo();
    }

    public allotmentTeamProfileInfo(){
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key'))
        })
      };
      this.http.get('http://'+this.url+'allotmentTeamProfileInfo', this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.allotmentTeamProfile=data.data[0];
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
    }
}
