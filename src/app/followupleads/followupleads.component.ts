import { Component, OnInit, ViewChild } from '@angular/core';
import { Jsonp } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-followupleads',
  templateUrl: './followupleads.component.html',
  styleUrls: ['./followupleads.component.css']
})
export class FollowupleadsComponent implements OnInit {
  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  error :any;
  allLeadsDetail : any;
  leadsDetailsById : any ={category:null, brand:null,description:null,date:null};
  customerAddressByLeadId : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  l_id:any;
  service_team : any = {name:null, email:null, st_phone:null, photo:null};
  //public tableData1: TableData;
  public showDialog : boolean;
  //public showDialogUpdate : boolean;
  public showDialogFollowUp:boolean;
  followupmsg : any;
  image : any;
  audio:any;
  c_phone:any;
  st_phone:any;
  @ViewChild('fileImg') myInputVariableImage: any;
  msg:any;
  @ViewChild('fileAudio') myInputVariableAudio:any;
  followUpDate:any;
  followUpLeadsDetail:any;
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    this.followUpLeads('',1,0);
    this.showDialog = false;
    //this.showDialogUpdate = false;
  }

  public followUpLeads(status,followup,reopen){
    this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          this.followUpLeadsDetail=null;
          //alert(data.message);
        }
        else{
          this.followUpLeadsDetail=data.data;
          //alert(data.message);
          //console.log(this.allLeadsDetail);
        }
      }

    });
  }

  public viewLead(l_id){
    this.l_id = l_id;
    this.data = "l_id="+l_id;
    this.showDialog = true;
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
    this.http.post('http://'+this.url+'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            //alert(data.message);
            //console.log(data);
            this.leadsDetailsById = data.lead_data[0];
            this.customerAddressByLeadId = data.customer_address[0];
            this.getServiceTeamInfoByLeadId();
            this.getFollowUpInfo(l_id);

          };
        }

    });
  }

  public getFollowUpInfo(l_id){
      this.data = "l_id="+l_id;

      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getFollowUpInfoAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.followupmsg=data.msg;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
  }

  public getServiceTeamInfoByLeadId(){
    //this.category = this.leadsDetailsById.category;
    //this.data = "category="+this.leadsDetailsById.category;
    this.data = "l_id="+this.leadsDetailsById.l_id;
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
    this.http.post('http://'+this.url+'getServiceTeamInfoByLeadIdAcceptedAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.service_team = data.service_team_info[0];
            console.log(data);
          };
        }

    });
  }
}
